package test;

import cn.com.openportal.ffw.modules.sys.entity.SysLogEntity;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package test
 * @create 2020-01-08 5:51
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class test {
    public static void main(String[] args) throws Exception {
    }

    public static void getType() {
        Field[] fs = BeanTest.class.getDeclaredFields();
        for (int i = 0; i < fs.length; i++) {
            String fieldName = fs[i].getName();
            String type = fs[i].getGenericType().toString();
            System.out.println(fieldName + " " + type);
            boolean state = false;
            if (type.equals("class java.lang.String")
                    || type.equals("class java.lang.Long")
                    || type.equals("long")
                    || type.equals("class java.lang.Integer")
                    || type.equals("int")
                    || type.equals("class java.lang.Boolean")
                    || type.equals("boolean")
            ) {
                state = true;
            }
            if (!state) {
                continue;
            }
            if (StringUtils.isBlank(fieldName)) {
                continue;
            }
            System.out.println("===can===");
        }
    }

    public static void maptoObj() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("username", "aaa");
        map.put("curPage", 1);
        map.put("limit", 10);

        SysLogEntity e = mapToObject(map, SysLogEntity.class);
        System.out.println(e);
    }

    public static <T> T mapToObject(Map<String, Object> map, Class<T> beanClass) throws Exception {
        if (map == null) {
            return null;
        }
        T obj = beanClass.newInstance();
        org.apache.commons.beanutils.BeanUtils.populate(obj, map);
        return obj;
    }

    public static void getField() {
        try {
            Class<?> clazz = SysLogEntity.class;// 获取PrivateClass整个类
            SysLogEntity pc = (SysLogEntity) clazz.newInstance();// 创建一个实例

            Field[] fs = clazz.getDeclaredFields();// 获取PrivateClass所有属性
            for (int i = 0; i < fs.length; i++) {
                fs[i].setAccessible(true);// 将目标属性设置为可以访问
                System.out.println("属性：" + fs[i].getName() + ":" + fs[i].get(pc));
            }

            System.out.println("========================================================");

            Method[] ms = clazz.getDeclaredMethods();// 获取PrivateClass所有的方法
            for (int i = 0; i < ms.length; i++) {
                ms[i].setAccessible(true);// 将目标属性设置为可以访问
                System.out.println("方法 ： " + ms[i].getName());//输出所以方法的名称
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

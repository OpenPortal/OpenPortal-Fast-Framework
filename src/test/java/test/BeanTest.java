package test;

import java.util.Date;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package test
 * @create 2020-01-08 7:02
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class BeanTest {
    private Long id;
    private long idl;
    private Integer age;
    private int agei;
    private String name;
    private boolean b;
    private Boolean bb;
    private Date date;
}

package test;

import cn.com.openportal.ffw.common.utils.GenericSuperclassUtil;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package test
 * @create 2020-01-08 9:29
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class BaseService<T> {

    public Class<?> getEntityClass() {
        return GenericSuperclassUtil.getActualTypeArgument(this.getClass());
    }

}

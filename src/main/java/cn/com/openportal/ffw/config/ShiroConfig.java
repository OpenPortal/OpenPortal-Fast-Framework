package cn.com.openportal.ffw.config;

import cn.com.openportal.ffw.cache.config.CacheType;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.crazycake.shiro.RedisCacheManager;
import cn.com.openportal.ffw.cache.shiro.ShiroCacheManager;
import cn.com.openportal.ffw.modules.sys.oauth2.OAuth2Filter;
import cn.com.openportal.ffw.modules.sys.oauth2.OAuth2Realm;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Shiro配置
 *
 * @author LeeSon
 */
@Configuration
public class ShiroConfig {

    @Bean("securityManager")
    public SecurityManager securityManager(ShiroCacheManager shiroCacheManager, SessionManager shiroSessionManager, OAuth2Realm oAuth2Realm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(oAuth2Realm);
        securityManager.setRememberMeManager(null);

        securityManager.setCacheManager(shiroCacheManager);
        securityManager.setSessionManager(shiroSessionManager);

        return securityManager;
    }

//    @Bean("securityManager")
//    public SecurityManager securityManager(CacheType shiroCacheType, EhCacheManager shiroEhCacheManager, RedisCacheManager shiroRedisCacheManager, SessionManager sessionManager, OAuth2Realm oAuth2Realm) {
//        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
//        securityManager.setRealm(oAuth2Realm);
//        securityManager.setRememberMeManager(null);
//        if (shiroCacheType.getType() == CacheType.CACHE_IS_REDIS) {
//            // Redis缓存实现
//            securityManager.setCacheManager(shiroRedisCacheManager);
//            securityManager.setSessionManager(sessionManager);
//        } else if (shiroCacheType.getType() == CacheType.CACHE_IS_EHCACHE) {
//            // ehCache缓存实现
//            securityManager.setCacheManager(shiroEhCacheManager);
//        } else if (shiroCacheType.getType() == CacheType.CACHE_IS_LOCALCACHE) {
//            // LocalCache缓存实现 ====>> Cache缓存实现
//            securityManager.setCacheManager(shiroEhCacheManager);
//        } else {
//            // 无缓存实现
//        }
//        return securityManager;
//    }

    @Bean("shiroFilter")
    public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
        shiroFilter.setSecurityManager(securityManager);

        //oauth过滤
        Map<String, Filter> filters = new HashMap<>(1);
        filters.put("oauth2", new OAuth2Filter());
        shiroFilter.setFilters(filters);

        Map<String, String> filterMap = new LinkedHashMap<>();
        filterMap.put("/actuator/**", "anon");
        filterMap.put("/webjars/**", "anon");
        filterMap.put("/druid/**", "anon");
        filterMap.put("/app/**", "anon");
        filterMap.put("/sys/login", "anon");
        filterMap.put("/swagger/**", "anon");
        filterMap.put("/v2/api-docs", "anon");
        filterMap.put("/swagger-ui.html", "anon");
        filterMap.put("/swagger-resources/**", "anon");
        filterMap.put("/captcha.jpg", "anon");
        filterMap.put("/aaa.txt", "anon");
        filterMap.put("/**", "oauth2");
        shiroFilter.setFilterChainDefinitionMap(filterMap);

        return shiroFilter;
    }

    @Bean("lifecycleBeanPostProcessor")
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }

}

package cn.com.openportal.ffw.config;

import cn.com.openportal.ffw.modules.sys.entity.SysMenuEntity;
import cn.com.openportal.ffw.modules.sys.service.SysMenuService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.config
 * @create 2020-01-17 0:34
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
@Configuration
public class AdminConfig {
    @Value("${spring.boot.admin.client.url}")
    private String adminUrl;
    @Value("${ffw.server}")
    private String serverUrl;
    @Value("${server.servlet.context-path: /}")
    private String pathUrl;
    @Value("${server.port: 80}")
    private String port;

    @Autowired
    private SysMenuService sysMenuService;

    @Bean
    public void OpenPortal_FFW_AdminConfig(){
        if(StringUtils.isNotBlank(serverUrl)&&StringUtils.isNotBlank(pathUrl)){
            SysMenuEntity menu = sysMenuService.getById(35L);
            String url=serverUrl+":"+port+pathUrl+"/druid/sql.html";
            menu.setUrl(url);
            sysMenuService.updateById(menu);
        }
        if(StringUtils.isNotBlank(adminUrl)){
            SysMenuEntity menu = sysMenuService.getById(36L);
            menu.setUrl(adminUrl);
            sysMenuService.updateById(menu);
        }
    }
}

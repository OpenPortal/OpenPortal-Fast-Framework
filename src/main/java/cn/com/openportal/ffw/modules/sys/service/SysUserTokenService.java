package cn.com.openportal.ffw.modules.sys.service;

import cn.com.openportal.ffw.common.utils.R;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.com.openportal.ffw.modules.sys.entity.SysUserTokenEntity;

/**
 * 用户Token
 *
 * @author LeeSon
 */
public interface SysUserTokenService extends IService<SysUserTokenEntity> {

	/**
	 * 生成token
	 * @param userId  用户ID
	 */
	R createToken(long userId);

	/**
	 * 退出，修改token值
	 * @param userId  用户ID
	 */
	void logout(long userId);

}

package cn.com.openportal.ffw.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.com.openportal.ffw.modules.sys.entity.SysUserRoleEntity;

import java.util.List;

/**
 * 用户与角色对应关系
 *
 * @author LeeSon
 */
public interface SysUserRoleService extends IService<SysUserRoleEntity> {

	void saveOrUpdate(Long userId, List<Long> roleIdList);

	/**
	 * 根据用户ID，获取角色ID列表
	 */
	List<Long> queryRoleIdList(Long userId);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);

	/**
	 * 根据角色ID，获取用户ID列表
	 */
	List<Long> queryUserIdList(Long roleId);

	/**
	 * 根据用户ID数组，批量删除
	 */
	int deleteBatchByUser(Long[] userIds);
}

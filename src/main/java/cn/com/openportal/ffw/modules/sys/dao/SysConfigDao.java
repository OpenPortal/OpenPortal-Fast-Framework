package cn.com.openportal.ffw.modules.sys.dao;

import cn.com.openportal.ffw.cache.mybatis.MybatisCache;
import cn.com.openportal.ffw.cache.mybatis.MybatisRedisCache;
import org.mybatis.caches.ehcache.EhcacheCache;
import cn.com.openportal.ffw.modules.sys.entity.SysConfigEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 系统配置信息
 *
 * @author LeeSon
 */
@CacheNamespace(implementation= MybatisCache.class,eviction=MybatisCache.class)
//@CacheNamespace(implementation= EhcacheCache.class,eviction=EhcacheCache.class)
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)

@Mapper
public interface SysConfigDao extends BaseMapper<SysConfigEntity> {

	/**
	 * 根据key，查询value
	 */
	SysConfigEntity queryByKey(String paramKey);

	/**
	 * 根据key，更新value
	 */
	int updateValueByKey(@Param("paramKey") String paramKey, @Param("paramValue") String paramValue);

}

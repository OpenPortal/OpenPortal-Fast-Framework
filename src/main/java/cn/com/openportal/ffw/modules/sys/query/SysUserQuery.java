package cn.com.openportal.ffw.modules.sys.query;

import cn.com.openportal.ffw.common.utils.BaseQuery;
import cn.com.openportal.ffw.modules.sys.entity.SysUserEntity;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @create 2020-01-08 10:13
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class SysUserQuery extends BaseQuery<SysUserEntity> {
}

package cn.com.openportal.ffw.modules.oss.query;

import cn.com.openportal.ffw.common.utils.BaseQuery;
import cn.com.openportal.ffw.modules.oss.entity.SysOssEntity;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @create 2020-01-08 10:11
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class SysOssQuery extends BaseQuery<SysOssEntity> {
}

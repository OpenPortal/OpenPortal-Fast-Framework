package cn.com.openportal.ffw.modules.sys.controller;

import cn.com.openportal.ffw.common.annotation.SysLog;
import cn.com.openportal.ffw.common.exception.RRException;
import cn.com.openportal.ffw.common.utils.Constant;
import cn.com.openportal.ffw.common.utils.PageUtils;
import cn.com.openportal.ffw.common.utils.R;
import cn.com.openportal.ffw.common.validator.Assert;
import cn.com.openportal.ffw.common.validator.ValidatorUtils;
import cn.com.openportal.ffw.common.validator.group.AddGroup;
import cn.com.openportal.ffw.common.validator.group.UpdateGroup;
import cn.com.openportal.ffw.modules.sys.entity.SysUserEntity;
import cn.com.openportal.ffw.modules.sys.form.PasswordForm;
import cn.com.openportal.ffw.modules.sys.service.SysDeptService;
import cn.com.openportal.ffw.modules.sys.service.SysUserRoleService;
import cn.com.openportal.ffw.modules.sys.service.SysUserService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 系统用户
 *
 * @author LeeSon
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;
    @Autowired
    private SysDeptService sysDeptService;


    /**
     * 所有用户列表
     */
    @GetMapping("/list")
    @RequiresPermissions("sys:user:list")
    public R list(@RequestParam Map<String, Object> params) {
        //只有超级管理员，才能查看所有管理员列表
        if (getUserId() != Constant.SUPER_ADMIN) {
            //params.put("createUserId", getUserId());
            List<Long> deptIds=sysDeptService.getSubDeptIdList(getUser().getDeptId());
            params.put("deptIds", deptIds);
        }

        PageUtils page = sysUserService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 获取登录的用户信息
     */
    @GetMapping("/info")
    public R info() {
        return R.ok().put("user", getUser());
    }

    /**
     * 修改登录用户密码
     */
    @SysLog("修改密码")
    @PostMapping("/password")
    public R password(@RequestBody PasswordForm form) {
        Assert.isBlank(form.getNewPassword(), "新密码不为能空");

        //sha256加密
        String password = new Sha256Hash(form.getPassword(), getUser().getSalt()).toHex();
        //sha256加密
        String newPassword = new Sha256Hash(form.getNewPassword(), getUser().getSalt()).toHex();

        //更新密码
        boolean flag = sysUserService.updatePassword(getUserId(), password, newPassword, form.getEmail(), form.getMobile());
        if (!flag) {
            return R.error("原密码不正确");
        }

        return R.ok();
    }

    /**
     * 用户信息
     */
    @GetMapping("/info/{userId}")
    @RequiresPermissions("sys:user:info")
    public R info(@PathVariable("userId") Long userId) {

        checkPrems(userId);

        SysUserEntity user = sysUserService.getById(userId);

        //获取用户所属的角色列表
        List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
        user.setRoleIdList(roleIdList);

        return R.ok().put("user", user);
    }

    /**
     * 保存用户
     */
    @SysLog("保存用户")
    @PostMapping("/save")
    @RequiresPermissions("sys:user:save")
    public R save(@RequestBody SysUserEntity user) {
        ValidatorUtils.validateEntity(user, AddGroup.class);

        user.setCreateUserId(getUserId());

        sysUserService.saveUser(user);

        return R.ok();
    }

    /**
     * 修改用户
     */
    @SysLog("修改用户")
    @PostMapping("/update")
    @RequiresPermissions("sys:user:update")
    public R update(@RequestBody SysUserEntity user) {

        checkPrems(user.getUserId());

        ValidatorUtils.validateEntity(user, UpdateGroup.class);

        user.setCreateUserId(sysUserService.getById(user.getUserId()).getCreateUserId());

        if(user.getUserId() == Constant.SUPER_ADMIN){
            user.setStatus(1);
        }

        sysUserService.update(user);

        return R.ok();
    }

    /**
     * 删除用户
     */
    @SysLog("删除用户")
    @PostMapping("/delete")
    @RequiresPermissions("sys:user:delete")
    public R delete(@RequestBody Long[] userIds) {
        if (ArrayUtils.contains(userIds, 1L)) {
            return R.error("系统管理员不能删除");
        }

        if (ArrayUtils.contains(userIds, getUserId())) {
            return R.error("当前用户不能删除");
        }

        for (Long userId : userIds) {
            checkPrems(userId);
        }

        sysUserService.deleteBatch(userIds);

        return R.ok();
    }

    /**
     * 检查权限
     */
    private void checkPrems(Long userId) {
        if (getUserId() != Constant.SUPER_ADMIN) {
            //如果不是超级管理员，则需要判断用户的角色是否自己创建
//            if (sysUserService.getById(userId).getCreateUserId().longValue() != getUserId().longValue()) {
//                throw new RRException("所选用户，不是本人创建");
//            }
            List<Long> deptIds=sysDeptService.getSubDeptIdList(getUser().getDeptId());
            SysUserEntity user=sysUserService.getById(userId);
            if(null!=user){
                Long deptIdF=user.getDeptId();
                if(null!=deptIdF){
                    if(!deptIds.contains(deptIdF)){
                        throw new RRException("权限限制");
                    }
                }
            }
        }
    }
}

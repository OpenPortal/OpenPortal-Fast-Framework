package cn.com.openportal.ffw.modules.portal.dao;

import cn.com.openportal.ffw.cache.mybatis.MybatisCache;
import cn.com.openportal.ffw.cache.mybatis.MybatisRedisCache;
import org.mybatis.caches.ehcache.EhcacheCache;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;
import java.sql.SQLException;
import java.util.List;

import cn.com.openportal.ffw.modules.portal.entity.PortalUser;
import cn.com.openportal.ffw.modules.portal.query.PortalUserQueryNative;


@CacheNamespace(implementation= MybatisCache.class,eviction=MybatisCache.class)
//@CacheNamespace(implementation= EhcacheCache.class,eviction=EhcacheCache.class)
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction= MybatisRedisCache.class)

@Mapper
public interface PortalUserDaoNative {

	/**
	 * 添加
	 */
	public Long addNative(PortalUser portalUser) throws SQLException;

	/**
	 * 根据主键查找
	 */
	public PortalUser getByKeyNative(Long id) throws SQLException;

	/**
	 * 根据主键批量查找
	 */
	public List<PortalUser> getByKeysNative(List<Long> idList) throws SQLException;

	/**
	 * 根据主键删除
	 */
	public Integer deleteByKeyNative(Long id) throws SQLException;

	/**
	 * 根据条件删除
	 */
	public Integer deleteByQueryNative(PortalUserQueryNative portalUserQueryNative) throws SQLException;

	/**
	 * 根据主键批量删除
	 */
	public Integer deleteByKeysNative(List<Long> idList) throws SQLException;

	/**
	 * 删除所有
	 */
	public Integer deleteAllNative() throws SQLException;

	/**
	 * 根据主键更新非空字段
	 */
	public Integer updateByKeyNative(PortalUser portalUser) throws SQLException;

	/**
	 * 根据主键更新所有字段
	 */
	public Integer updateByKeyAllNative(PortalUser portalUser) throws SQLException;

	/**
	 * 分页查询
	 */
	public List<PortalUser> getPageNative(PortalUserQueryNative portalUserQueryNative) throws SQLException;

	/**
	 * 集合查询
	 */
	public List<PortalUser> getListNative(PortalUserQueryNative portalUserQueryNative) throws SQLException;

	/**
	 * 总条数
	 */
	public Integer getCountNative(PortalUserQueryNative portalUserQueryNative) throws SQLException;

}

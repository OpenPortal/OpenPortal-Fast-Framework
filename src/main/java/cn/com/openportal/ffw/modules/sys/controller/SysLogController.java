package cn.com.openportal.ffw.modules.sys.controller;

import cn.com.openportal.ffw.common.annotation.SysLog;
import cn.com.openportal.ffw.common.utils.PageUtils;
import cn.com.openportal.ffw.common.utils.R;
import cn.com.openportal.ffw.modules.sys.service.SysLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 系统日志
 *
 * @author LeeSon
 */
@Controller
@RequestMapping("/sys/log")
public class SysLogController {
	@Autowired
	private SysLogService sysLogService;

	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("sys:log:list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = sysLogService.queryPage(params);

		return R.ok().put("page", page);
	}

	/**
	 * 全部删除
	 */
	@ResponseBody
	@SysLog("删除日志")
	@GetMapping("/delete")
	@RequiresPermissions("sys:log:delete")
	public R delete(){
		sysLogService.remove();
		return R.ok("删除成功");
	}

}

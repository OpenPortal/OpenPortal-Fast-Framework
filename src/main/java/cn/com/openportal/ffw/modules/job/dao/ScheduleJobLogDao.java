package cn.com.openportal.ffw.modules.job.dao;

import cn.com.openportal.ffw.cache.mybatis.MybatisCache;
import cn.com.openportal.ffw.cache.mybatis.MybatisRedisCache;
import org.mybatis.caches.ehcache.EhcacheCache;
import cn.com.openportal.ffw.modules.job.entity.ScheduleJobLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;

/**
 * 定时任务日志
 *
 * @author LeeSon
 */
@CacheNamespace(implementation= MybatisCache.class,eviction=MybatisCache.class)
//@CacheNamespace(implementation= EhcacheCache.class,eviction=EhcacheCache.class)
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)

@Mapper
public interface ScheduleJobLogDao extends BaseMapper<ScheduleJobLogEntity> {

}

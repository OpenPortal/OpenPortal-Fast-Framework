package cn.com.openportal.ffw.modules.job.service;

import cn.com.openportal.ffw.common.utils.PageUtils;
import cn.com.openportal.ffw.modules.job.entity.ScheduleJobLogEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 定时任务日志
 *
 * @author LeeSon
 */
public interface ScheduleJobLogService extends IService<ScheduleJobLogEntity> {

	PageUtils queryPage(Map<String, Object> params);

}

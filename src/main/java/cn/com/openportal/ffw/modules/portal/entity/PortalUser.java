package cn.com.openportal.ffw.modules.portal.entity;

import cn.com.openportal.ffw.common.utils.DateUtils;
import cn.com.openportal.ffw.common.validator.group.AddGroup;
import cn.com.openportal.ffw.common.validator.group.UpdateGroup;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.io.Serializable;

/**
 * 实体类对象
 *
 * @author LeeSon
 */
public class PortalUser implements Serializable {
    /**
     * 序列化ID
     */
    private static final long serialVersionUID = 1L;

    @NotNull(message = "参数错误", groups = UpdateGroup.class)
    private Long id;

    @NotBlank(message = "用户名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String username;

    @NotBlank(message = "密码不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String password;

    @NotBlank(message = "邮箱不能为空", groups = {AddGroup.class, UpdateGroup.class})
    @Email(message = "邮箱格式不正确", groups = {AddGroup.class, UpdateGroup.class})
    private String email;

    @NotBlank(message = "电话不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String mobile;

    @NotNull(message = "年龄不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Integer age;

    @ApiModelProperty(required = true, example = "2020-01-01 11:11:11")
    @JsonFormat(pattern = DateUtils.DATE_TIME_PATTERN)
    @DateTimeFormat(pattern = DateUtils.DATE_TIME_PATTERN)
    private Date createTime;

    private Long createUserId;
    private Long deptId;

    @ApiModelProperty(hidden = true) //swagger不显示
    private String deptName;

    @ApiModelProperty(hidden = true) //swagger不显示
    private String createUserName;

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    @Override
    public String toString() {
        return "PortalUser{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                ", age=" + age +
                ", createTime=" + createTime +
                ", createUserId=" + createUserId +
                ", deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", createUserName='" + createUserName + '\'' +
                '}';
    }
}

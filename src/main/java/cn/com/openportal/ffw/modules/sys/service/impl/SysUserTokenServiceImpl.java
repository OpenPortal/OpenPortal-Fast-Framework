package cn.com.openportal.ffw.modules.sys.service.impl;

import cn.com.openportal.ffw.cache.utils.CacheUtils;
import cn.com.openportal.ffw.common.utils.Constant;
import cn.com.openportal.ffw.common.utils.R;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.com.openportal.ffw.modules.sys.dao.SysUserTokenDao;
import cn.com.openportal.ffw.modules.sys.entity.SysUserTokenEntity;
import cn.com.openportal.ffw.modules.sys.oauth2.TokenGenerator;
import cn.com.openportal.ffw.modules.sys.service.SysUserTokenService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("sysUserTokenService")
public class SysUserTokenServiceImpl extends ServiceImpl<SysUserTokenDao, SysUserTokenEntity> implements SysUserTokenService {
    //12小时后过期
    private final static int EXPIRE = 3600 * 12;

    @Autowired
    private CacheUtils CacheUtils;

    @Override
    public R createToken(long userId) {
        //生成一个token
        String token = TokenGenerator.generateValue();

        //当前时间
        Date now = new Date();
        //过期时间
        Date expireTime = new Date(now.getTime() + EXPIRE * 1000);

        //判断是否生成过token
        SysUserTokenEntity tokenEntity = this.getById(userId);
        if (tokenEntity == null) {
            tokenEntity = new SysUserTokenEntity();
            tokenEntity.setUserId(userId);
            tokenEntity.setToken(token);
            tokenEntity.setUpdateTime(now);
            tokenEntity.setExpireTime(expireTime);

            //保存token
            this.save(tokenEntity);

            //CacheUtils.delCache(Constant.SYSUSERTOKENENTITY, "createToken");
        } else {
            String oldToken = tokenEntity.getToken();

            tokenEntity.setToken(token);
            tokenEntity.setUpdateTime(now);
            tokenEntity.setExpireTime(expireTime);

            //更新token
            this.updateById(tokenEntity);

            //CacheUtils.delCache(Constant.SYSUSERTOKENENTITY, "updateToken");
            if (StringUtils.isNotBlank(oldToken)) {
                CacheUtils.removeCacheKey(Constant.SYSUSERTOKENENTITY, oldToken, "updateToken");
            }
        }

        R r = R.ok().put("token", token).put("expire", EXPIRE);

        return r;
    }

    @Override
    public void logout(long userId) {
        //生成一个token
        String token = TokenGenerator.generateValue();

        //判断是否生成过token
        SysUserTokenEntity oldTokenEntity = this.getById(userId);
        String oldToken = null;
        if (null != oldTokenEntity) {
            oldToken = oldTokenEntity.getToken();
        }

        //修改token
        SysUserTokenEntity tokenEntity = new SysUserTokenEntity();
        tokenEntity.setUserId(userId);
        tokenEntity.setToken(token);
        this.updateById(tokenEntity);

        //CacheUtils.delCache(Constant.SYSUSERTOKENENTITY, "logout");
        if (StringUtils.isNotBlank(oldToken)) {
            CacheUtils.removeCacheKey(Constant.SYSUSERTOKENENTITY, oldToken, "logout");
        }
    }
}

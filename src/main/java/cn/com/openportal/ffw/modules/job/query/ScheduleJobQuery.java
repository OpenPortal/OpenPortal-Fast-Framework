package cn.com.openportal.ffw.modules.job.query;

import cn.com.openportal.ffw.common.utils.BaseQuery;
import cn.com.openportal.ffw.modules.job.entity.ScheduleJobEntity;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @create 2020-01-08 10:08
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class ScheduleJobQuery extends BaseQuery<ScheduleJobEntity> {
}

package cn.com.openportal.ffw.modules.portal.controller;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import cn.com.openportal.ffw.cache.utils.CachePrintTest;
import cn.com.openportal.ffw.common.annotation.DataFilterNative;
import cn.com.openportal.ffw.common.utils.BaseEntitySetValueUtils;
import cn.com.openportal.ffw.common.validator.Assert;
import cn.com.openportal.ffw.common.validator.ValidatorUtils;
import cn.com.openportal.ffw.common.validator.group.AddGroup;
import cn.com.openportal.ffw.common.validator.group.UpdateGroup;
import cn.com.openportal.ffw.modules.sys.controller.AbstractController;
import org.apache.commons.io.FileUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import cn.com.openportal.ffw.common.annotation.SysLog;
import cn.com.openportal.ffw.common.utils.PageUtils;
import cn.com.openportal.ffw.common.utils.R;

import cn.com.openportal.ffw.modules.portal.entity.PortalUser;
import cn.com.openportal.ffw.modules.portal.query.PortalUserQueryNative;
import cn.com.openportal.ffw.modules.portal.service.PortalUserServiceNative;

/**
 * PortalUserControllerNative
 * 控制层模板
 *
 * @author LeeSon
 */
@Api("PortalUser 接口")
@Controller
@RequestMapping("portal/portaluser")
public class PortalUserControllerNative extends AbstractController {

    @Autowired
    private PortalUserServiceNative portalUserServiceNative;
    @Autowired
    private BaseEntitySetValueUtils baseEntitySetValueUtils;

    /**
     * 文件下载
     */
    @RequiresPermissions("portal:portaluser:list")
    @RequestMapping(value = "/download")
    public ResponseEntity<byte[]> download() throws IOException {
        //ResponseEntity<byte[]> result=downloadFile();
        ResponseEntity<byte[]> result = downloadText();
        return result;
    }

    private ResponseEntity<byte[]> downloadText() {
        String test = "hello,Download test ...";
        ResponseEntity.BodyBuilder builder = ResponseEntity.ok();
        builder.contentLength(test.length());
        builder.contentType(MediaType.APPLICATION_OCTET_STREAM);
        builder.header("Content-Disposition", "attachment; filename=test.txt");
        return builder.body(test.getBytes());
    }

    private ResponseEntity<byte[]> downloadFile() throws IOException {
        String fileName = "test.txt";
        String path = "d:";

        File file = new File(path, fileName);
        ResponseEntity.BodyBuilder builder = ResponseEntity.ok();
        builder.contentLength(file.length());
        builder.contentType(MediaType.APPLICATION_OCTET_STREAM);
        builder.header("Content-Disposition", "attachment; filename=" + new String(fileName.getBytes(), "utf-8"));
        return builder.body(FileUtils.readFileToByteArray(file));
    }

    /**
     * 分页列表
     */
    @DataFilterNative(subDept = true, user = false)
    @ApiOperation("分页查询")
    @ResponseBody
    @RequiresPermissions("portal:portaluser:list")
    @PostMapping("/list")
    public R page(PortalUserQueryNative query) {
        PageUtils page = portalUserServiceNative.getPageNative(query);
        List<PortalUser> list = (List<PortalUser>) page.getList();
        baseEntitySetValueUtils.BaseEntitySetValue(list);

        return R.ok().put("page", page);
    }

    /**
     * 获取
     */
    @DataFilterNative(subDept = true, user = false)
    @ApiOperation("根据ID查询 RESTful")
    @ResponseBody
    @RequiresPermissions("portal:portaluser:info")
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
        PortalUser e = portalUserServiceNative.getByKeyNative(id);
        //CachePrintTest.getCache();
        return R.ok().put("portalUser", e);
    }

    /**
     * 添加
     */
    @ApiOperation("添加")
    @ResponseBody
    @SysLog("add portalUser")
    @RequiresPermissions("portal:portaluser:save")
    @PostMapping("/save")
    public R save(@RequestBody PortalUser e) {

        ValidatorUtils.validateEntity(e, AddGroup.class);

        e.setCreateUserId(getUserId());
        e.setDeptId(getUser().getDeptId());
        e.setCreateTime(new Date());

        portalUserServiceNative.addNative(e);
        return R.ok();
    }

    /**
     * 修改
     */
    @DataFilterNative(subDept = true, user = false)
    @ApiOperation("修改")
    @ResponseBody
    @SysLog("update portalUser")
    @RequiresPermissions("portal:portaluser:update")
    @PostMapping("/update")
    public R update(@RequestBody PortalUser e) {

        ValidatorUtils.validateEntity(e, UpdateGroup.class);

        PortalUser t = portalUserServiceNative.getByKeyNative(e.getId());
        if(null!=t){
            e.setCreateUserId(t.getCreateUserId());
            e.setDeptId(t.getDeptId());
            e.setCreateTime(t.getCreateTime());
        }

        portalUserServiceNative.updateByKeyNative(e);
        return R.ok();
    }

    /**
     * 全匹配修改
     */
    @DataFilterNative(subDept = true, user = false)
    @ApiOperation("全匹配修改")
    @ResponseBody
    @SysLog("updateMate portalUser")
    @RequiresPermissions("portal:portaluser:update")
    @PostMapping("/updateMate")
    public R updateMate(@RequestBody PortalUser e) {

        Assert.isNull(e.getId(), "参数错误");

        PortalUser t = portalUserServiceNative.getByKeyNative(e.getId());
        if(null!=t){
            e.setCreateUserId(t.getCreateUserId());
            e.setDeptId(t.getDeptId());
            e.setCreateTime(t.getCreateTime());
        }

        portalUserServiceNative.updateByKeyAllNative(e);
        return R.ok();
    }

    /**
     * 单个删除
     */
    @DataFilterNative(subDept = true, user = true)
    @ApiOperation("根据ID单个删除")
    @ResponseBody
    @SysLog("delete id portalUser")
    @RequiresPermissions("portal:portaluser:delete")
    @GetMapping("/delete")
    public R delete(@RequestParam Long id) {
        portalUserServiceNative.deleteByKeyNative(id);
        return R.ok();
    }

    /**
     * 删除多个
     */
    @DataFilterNative(subDept = true, user = true)
    @ApiOperation("根据IDS批量删除")
    @ResponseBody
    @SysLog("delete ids portalUser")
    @RequiresPermissions("portal:portaluser:delete")
    @PostMapping("/deletes")
    public R deletes(@RequestParam Long[] ids) {
        //if(ArrayUtils.contains(ids, 0L)){
        //    return R.error("该记录不能删除");
        //}
        List<Long> list = Arrays.asList(ids);
        portalUserServiceNative.deleteByKeysNative(list);
        return R.ok();
    }

    /**
     * 删除多个
     */
    @DataFilterNative(subDept = true, user = true)
    @ApiOperation("根据IDS RequestBody批量删除[id1,id2...]")
    @ResponseBody
    @SysLog("delete ids portalUser")
    @RequiresPermissions("portal:portaluser:delete")
    @PostMapping("/delete")
    public R deletesJSON(@RequestBody Long[] ids) {
        //if(ArrayUtils.contains(ids, 0L)){
        //    return R.error("该记录不能删除");
        //}
        List<Long> list = Arrays.asList(ids);
        portalUserServiceNative.deleteByKeysNative(list);
        return R.ok();
    }

    /**
     * 全部删除
     */
    @DataFilterNative(subDept = true, user = true)
    @ApiOperation("全部删除")
    @ResponseBody
    @SysLog("delete all portalUser")
    @RequiresPermissions("portal:portaluser:delete")
    @GetMapping("/deleteAll")
    public R deleteAll() {
        portalUserServiceNative.deleteAllNative();
        return R.ok();
    }

    /**
     * 条件删除
     */
    @DataFilterNative(subDept = true, user = true)
    @ApiOperation("条件删除")
    @ResponseBody
    @SysLog("delete query portalUser")
    @RequiresPermissions("portal:portaluser:delete")
    @PostMapping("/deleteQuery")
    public R deleteQuery(PortalUserQueryNative query) {
        portalUserServiceNative.deleteByQueryNative(query);
        return R.ok();
    }

}


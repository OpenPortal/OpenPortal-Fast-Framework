package cn.com.openportal.ffw.modules.sys.service.impl;

import cn.com.openportal.ffw.cache.utils.CacheUtils;
import cn.com.openportal.ffw.common.utils.Constant;
import cn.com.openportal.ffw.modules.sys.dao.SysMenuDao;
import cn.com.openportal.ffw.modules.sys.dao.SysUserDao;
import cn.com.openportal.ffw.modules.sys.dao.SysUserTokenDao;
import cn.com.openportal.ffw.modules.sys.entity.SysMenuEntity;
import cn.com.openportal.ffw.modules.sys.entity.SysUserEntity;
import cn.com.openportal.ffw.modules.sys.entity.SysUserTokenEntity;
import cn.com.openportal.ffw.modules.sys.service.ShiroService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ShiroServiceImpl implements ShiroService {

    @Autowired
    private SysMenuDao sysMenuDao;
    @Autowired
    private SysUserDao sysUserDao;
    @Autowired
    private SysUserTokenDao sysUserTokenDao;
    @Autowired
    private CacheUtils CacheUtils;

    @Override
    public Set<String> getUserPermissions(long userId) {
        List<String> permsList;

        //系统管理员，拥有最高权限
        if (userId == Constant.SUPER_ADMIN) {
            List<SysMenuEntity> menuList = sysMenuDao.selectList(null);
            permsList = new ArrayList<>(menuList.size());
            for (SysMenuEntity menu : menuList) {
                permsList.add(menu.getPerms());
            }
        } else {
            permsList = sysUserDao.queryAllPerms(userId);
        }
        //用户权限列表
        Set<String> permsSet = new HashSet<>();
        for (String perms : permsList) {
            if (StringUtils.isBlank(perms)) {
                continue;
            }
            permsSet.addAll(Arrays.asList(perms.trim().split(",")));
        }
        return permsSet;
    }

    @Override
    public SysUserTokenEntity queryByToken(String token) {
        if (StringUtils.isNotBlank(token)) {
            Object obj = CacheUtils.getCache(Constant.SYSUSERTOKENENTITY, token);
            if (null != obj) {
                return (SysUserTokenEntity) obj;
            }
        }
        SysUserTokenEntity e = sysUserTokenDao.queryByToken(token);
        if (StringUtils.isNotBlank(token) && null != e) {
            CacheUtils.putCache(Constant.SYSUSERTOKENENTITY, token, e);
        }
        return e;
    }

    @Override
    public SysUserEntity queryUser(Long userId) {
        if (null != userId) {
            Object obj = CacheUtils.getCache(Constant.SYSUSERENTITY, userId.toString());
            if (null != obj) {
                return (SysUserEntity) obj;
            }
        }
        SysUserEntity e = sysUserDao.selectById(userId);
        if (null != userId && null != e) {
            CacheUtils.putCache(Constant.SYSUSERENTITY, userId.toString(), e);
        }
        return e;
    }
}

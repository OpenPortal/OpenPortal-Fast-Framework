package cn.com.openportal.ffw.modules.sys.dao;

import cn.com.openportal.ffw.cache.mybatis.MybatisCache;
import cn.com.openportal.ffw.cache.mybatis.MybatisRedisCache;
import org.mybatis.caches.ehcache.EhcacheCache;
import cn.com.openportal.ffw.modules.sys.entity.SysDeptEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import java.util.Map;

/**
 * 部门管理
 *
 * @author LeeSon
 */
@CacheNamespace(implementation= MybatisCache.class,eviction=MybatisCache.class)
//@CacheNamespace(implementation= EhcacheCache.class,eviction=EhcacheCache.class)
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)

@Mapper
public interface SysDeptDao extends BaseMapper<SysDeptEntity> {

    List<SysDeptEntity> queryList(Map<String, Object> params);

    /**
     * 查询子部门ID列表
     * @param parentId  上级部门ID
     */
    List<Long> queryDetpIdList(Long parentId);

}

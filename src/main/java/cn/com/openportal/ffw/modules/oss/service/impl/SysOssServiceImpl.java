package cn.com.openportal.ffw.modules.oss.service.impl;

import cn.com.openportal.ffw.common.utils.PageUtils;
import cn.com.openportal.ffw.modules.oss.entity.SysOssEntity;
import cn.com.openportal.ffw.modules.oss.query.SysOssQuery;
import cn.com.openportal.ffw.modules.oss.service.SysOssService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.com.openportal.ffw.modules.oss.dao.SysOssDao;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author LeeSon
 */
@Service("sysOssService")
public class SysOssServiceImpl extends ServiceImpl<SysOssDao, SysOssEntity> implements SysOssService {

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		IPage<SysOssEntity> page = this.page(
			new SysOssQuery().getPage(params)
		);

		return new PageUtils(page);
	}

}

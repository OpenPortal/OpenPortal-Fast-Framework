package cn.com.openportal.ffw.modules.portal.query;

import java.util.*;

import io.swagger.annotations.ApiModelProperty;
import cn.com.openportal.ffw.common.utils.BaseQueryNative;

/**
 * 查询条件对象
 *
 * @author LeeSon
 */
public class PortalUserQueryNative extends BaseQueryNative {
    /**
     * ==============================批量查询、更新、删除时的Where条件设置======================
     **/
    private Long id;

    public Long getId() {
        return id;
    }

    public PortalUserQueryNative setId(Long id) {
        this.id = id;
        return this;
    }

    private String username;

    public String getUsername() {
        return username;
    }

    public PortalUserQueryNative setUsername(String username) {
        this.username = username;
        return this;
    }

    @ApiModelProperty(value = "是否模糊匹配 默认否")
    private boolean usernameLike;

    public PortalUserQueryNative setUsernameLike(boolean isLike) {
        this.usernameLike = isLike;
        return this;
    }

    public boolean isUsernameLike() {
        return usernameLike;
    }

    private String password;

    public String getPassword() {
        return password;
    }

    public PortalUserQueryNative setPassword(String password) {
        this.password = password;
        return this;
    }

    @ApiModelProperty(value = "是否模糊匹配 默认否")
    private boolean passwordLike;

    public PortalUserQueryNative setPasswordLike(boolean isLike) {
        this.passwordLike = isLike;
        return this;
    }

    public boolean isPasswordLike() {
        return passwordLike;
    }

    private String email;

    public String getEmail() {
        return email;
    }

    public PortalUserQueryNative setEmail(String email) {
        this.email = email;
        return this;
    }

    @ApiModelProperty(value = "是否模糊匹配 默认否")
    private boolean emailLike;

    public PortalUserQueryNative setEmailLike(boolean isLike) {
        this.emailLike = isLike;
        return this;
    }

    public boolean isEmailLike() {
        return emailLike;
    }

    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public PortalUserQueryNative setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    @ApiModelProperty(value = "是否模糊匹配 默认否")
    private boolean mobileLike;

    public PortalUserQueryNative setMobileLike(boolean isLike) {
        this.mobileLike = isLike;
        return this;
    }

    public boolean isMobileLike() {
        return mobileLike;
    }

    private Integer age;

    public Integer getAge() {
        return age;
    }

    public PortalUserQueryNative setAge(Integer age) {
        this.age = age;
        return this;
    }

    private Date createTime;

    public Date getCreateTime() {
        return createTime;
    }

    public PortalUserQueryNative setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    private Long createUserId;

    public Long getCreateUserId() {
        return createUserId;
    }

    public PortalUserQueryNative setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    private Long deptId;

    public Long getDeptId() {
        return deptId;
    }

    public PortalUserQueryNative setDeptId(Long deptId) {
        this.deptId = deptId;
        return this;
    }

    /**
     * ==============================批量查询时的Order条件顺序设置==========================
     **/
    public class OrderField {
        public OrderField(String fieldName, String order) {
            super();
            this.fieldName = fieldName;
            this.order = order;
        }

        private String fieldName;
        private String order;

        public String getFieldName() {
            return fieldName;
        }

        public OrderField setFieldName(String fieldName) {
            this.fieldName = fieldName;
            return this;
        }

        public String getOrder() {
            return order;
        }

        public OrderField setOrder(String order) {
            this.order = order;
            return this;
        }

        @Override
        public String toString() {
            return "OrderField{" +
                    "fieldName='" + fieldName + '\'' +
                    ", order='" + order + '\'' +
                    '}';
        }
    }

    /**
     * ==============================批量查询时的Order条件顺序设置==========================
     **/

    /**
     * 排序列表字段
     **/
    private List<OrderField> orderFields = new ArrayList<OrderField>();

    /**
     * 设置排序按属性：id
     *
     * @param isAsc 是否升序，否则为降序
     */
    public PortalUserQueryNative orderbyId(boolean isAsc) {
        orderFields.add(new OrderField("id", isAsc ? "ASC" : "DESC"));
        return this;
    }

    @ApiModelProperty(value = "排序desc 默认asc")
    private String id_order_by;

    public void setId_order_by(String id_order_by) {
        this.id_order_by = id_order_by;
        if (null != id_order_by && id_order_by.length() > 0) {
            if ("asc".equals(id_order_by)) {
                orderbyId(true);
            } else {
                orderbyId(false);
            }
        }
    }

    public String getId_order_by() {
        return id_order_by;
    }


    /**
     * 设置排序按属性：username
     *
     * @param isAsc 是否升序，否则为降序
     */
    public PortalUserQueryNative orderbyUsername(boolean isAsc) {
        orderFields.add(new OrderField("username", isAsc ? "ASC" : "DESC"));
        return this;
    }

    @ApiModelProperty(value = "排序desc 默认asc")
    private String username_order_by;

    public void setUsername_order_by(String username_order_by) {
        this.username_order_by = username_order_by;
        if (null != username_order_by && username_order_by.length() > 0) {
            if ("asc".equals(username_order_by)) {
                orderbyUsername(true);
            } else {
                orderbyUsername(false);
            }
        }
    }

    public String getUsername_order_by() {
        return username_order_by;
    }


    /**
     * 设置排序按属性：password
     *
     * @param isAsc 是否升序，否则为降序
     */
    public PortalUserQueryNative orderbyPassword(boolean isAsc) {
        orderFields.add(new OrderField("password", isAsc ? "ASC" : "DESC"));
        return this;
    }

    @ApiModelProperty(value = "排序desc 默认asc")
    private String password_order_by;

    public void setPassword_order_by(String password_order_by) {
        this.password_order_by = password_order_by;
        if (null != password_order_by && password_order_by.length() > 0) {
            if ("asc".equals(password_order_by)) {
                orderbyPassword(true);
            } else {
                orderbyPassword(false);
            }
        }
    }

    public String getPassword_order_by() {
        return password_order_by;
    }


    /**
     * 设置排序按属性：email
     *
     * @param isAsc 是否升序，否则为降序
     */
    public PortalUserQueryNative orderbyEmail(boolean isAsc) {
        orderFields.add(new OrderField("email", isAsc ? "ASC" : "DESC"));
        return this;
    }

    @ApiModelProperty(value = "排序desc 默认asc")
    private String email_order_by;

    public void setEmail_order_by(String email_order_by) {
        this.email_order_by = email_order_by;
        if (null != email_order_by && email_order_by.length() > 0) {
            if ("asc".equals(email_order_by)) {
                orderbyEmail(true);
            } else {
                orderbyEmail(false);
            }
        }
    }

    public String getEmail_order_by() {
        return email_order_by;
    }


    /**
     * 设置排序按属性：mobile
     *
     * @param isAsc 是否升序，否则为降序
     */
    public PortalUserQueryNative orderbyMobile(boolean isAsc) {
        orderFields.add(new OrderField("mobile", isAsc ? "ASC" : "DESC"));
        return this;
    }

    @ApiModelProperty(value = "排序desc 默认asc")
    private String mobile_order_by;

    public void setMobile_order_by(String mobile_order_by) {
        this.mobile_order_by = mobile_order_by;
        if (null != mobile_order_by && mobile_order_by.length() > 0) {
            if ("asc".equals(mobile_order_by)) {
                orderbyMobile(true);
            } else {
                orderbyMobile(false);
            }
        }
    }

    public String getMobile_order_by() {
        return mobile_order_by;
    }


    /**
     * 设置排序按属性：age
     *
     * @param isAsc 是否升序，否则为降序
     */
    public PortalUserQueryNative orderbyAge(boolean isAsc) {
        orderFields.add(new OrderField("age", isAsc ? "ASC" : "DESC"));
        return this;
    }

    @ApiModelProperty(value = "排序desc 默认asc")
    private String age_order_by;

    public void setAge_order_by(String age_order_by) {
        this.age_order_by = age_order_by;
        if (null != age_order_by && age_order_by.length() > 0) {
            if ("asc".equals(age_order_by)) {
                orderbyAge(true);
            } else {
                orderbyAge(false);
            }
        }
    }

    public String getAge_order_by() {
        return age_order_by;
    }


    /**
     * 设置排序按属性：create_time
     *
     * @param isAsc 是否升序，否则为降序
     */
    public PortalUserQueryNative orderbyCreateTime(boolean isAsc) {
        orderFields.add(new OrderField("create_time", isAsc ? "ASC" : "DESC"));
        return this;
    }

    @ApiModelProperty(value = "排序desc 默认asc")
    private String createTime_order_by;

    public void setCreateTime_order_by(String createTime_order_by) {
        this.createTime_order_by = createTime_order_by;
        if (null != createTime_order_by && createTime_order_by.length() > 0) {
            if ("asc".equals(createTime_order_by)) {
                orderbyCreateTime(true);
            } else {
                orderbyCreateTime(false);
            }
        }
    }

    public String getCreateTime_order_by() {
        return createTime_order_by;
    }


    /**
     * 设置排序按属性：create_user_id
     *
     * @param isAsc 是否升序，否则为降序
     */
    public PortalUserQueryNative orderbyCreateUserId(boolean isAsc) {
        orderFields.add(new OrderField("create_user_id", isAsc ? "ASC" : "DESC"));
        return this;
    }

    @ApiModelProperty(value = "排序desc 默认asc")
    private String createUserId_order_by;

    public void setCreateUserId_order_by(String createUserId_order_by) {
        this.createUserId_order_by = createUserId_order_by;
        if (null != createUserId_order_by && createUserId_order_by.length() > 0) {
            if ("asc".equals(createUserId_order_by)) {
                orderbyCreateUserId(true);
            } else {
                orderbyCreateUserId(false);
            }
        }
    }

    public String getCreateUserId_order_by() {
        return createUserId_order_by;
    }


    /**
     * 设置排序按属性：dept_id
     *
     * @param isAsc 是否升序，否则为降序
     */
    public PortalUserQueryNative orderbyDeptId(boolean isAsc) {
        orderFields.add(new OrderField("dept_id", isAsc ? "ASC" : "DESC"));
        return this;
    }

    @ApiModelProperty(value = "排序desc 默认asc")
    private String deptId_order_by;

    public void setDeptId_order_by(String deptId_order_by) {
        this.deptId_order_by = deptId_order_by;
        if (null != deptId_order_by && deptId_order_by.length() > 0) {
            if ("asc".equals(deptId_order_by)) {
                orderbyDeptId(true);
            } else {
                orderbyDeptId(false);
            }
        }
    }

    public String getDeptId_order_by() {
        return deptId_order_by;
    }


    /**
     * 提供自定义字段使用
     */
    private static Map<String, String> fieldMap;

    private static Map<String, String> getFieldSet() {
        if (fieldMap == null) {
            fieldMap = new HashMap<String, String>();
            fieldMap.put("id", "id");
            fieldMap.put("username", "username");
            fieldMap.put("password", "password");
            fieldMap.put("email", "email");
            fieldMap.put("mobile", "mobile");
            fieldMap.put("age", "age");
            fieldMap.put("createTime", "create_time");
            fieldMap.put("createUserId", "create_user_id");
            fieldMap.put("deptId", "dept_id");
        }
        return fieldMap;
    }

    @Override
    public String getFields() {
        return this.fields;
    }

    @Override
    public void setFields(String fields) {
        if (fields == null) {
            return;
        }
        String[] array = fields.split(",");
        StringBuffer buffer = new StringBuffer();
        for (String field : array) {
            if (getFieldSet().containsKey(field)) {
                buffer.append(getFieldSet().get(field)).append(" as ")
                        .append(field).append(" ,");
            }
            if (getFieldSet().containsKey("`" + field + "`")) {
                buffer.append("`" + getFieldSet().get(field) + "`").append(" as ")
                        .append(field).append(" ,");
            }
        }
        if (buffer.length() != 0) {
            this.fields = buffer.substring(0, buffer.length() - 1);
        } else {
            this.fields = " 1 ";// 没有一个参数可能会报错
        }
    }

    /**
     * 如果使用CacheAspect需要生成toString()方法，并且不要orderFields
     */
    @Override
    public String toString() {
        return "PortalUserQueryNative {" +
                "Entity [" +
                ", id=" + id +
                ", id_order_by=" + id_order_by +
                ", username=" + username +
                ", username_order_by=" + username_order_by +
                ", usernameLike=" + usernameLike +
                ", password=" + password +
                ", password_order_by=" + password_order_by +
                ", passwordLike=" + passwordLike +
                ", email=" + email +
                ", email_order_by=" + email_order_by +
                ", emailLike=" + emailLike +
                ", mobile=" + mobile +
                ", mobile_order_by=" + mobile_order_by +
                ", mobileLike=" + mobileLike +
                ", age=" + age +
                ", age_order_by=" + age_order_by +
                ", createTime=" + createTime +
                ", createTime_order_by=" + createTime_order_by +
                ", createUserId=" + createUserId +
                ", createUserId_order_by=" + createUserId_order_by +
                ", deptId=" + deptId +
                ", deptId_order_by=" + deptId_order_by +
                "]" +
//				", orderFields=" + orderFields +
                ", page=" + page +
                ", pageSize=" + pageSize +
                ", limit=" + limit +
                ", startRow=" + startRow +
                ", endRow=" + endRow +
                ", fields='" + fields + '\'' +
                ", begin_time=" + begin_time +
                ", end_time=" + end_time +
                ", begin_time1=" + begin_time1 +
                ", end_time1=" + end_time1 +
                ", gidList=" + gidList +
                '}';
    }
}

package cn.com.openportal.ffw.modules.sys.service.impl;

import cn.com.openportal.ffw.common.annotation.DataFilter;
import cn.com.openportal.ffw.modules.sys.dao.SysDeptDao;
import cn.com.openportal.ffw.modules.sys.entity.SysDeptEntity;
import cn.com.openportal.ffw.modules.sys.service.SysDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("sysDeptService")
public class SysDeptServiceImpl extends ServiceImpl<SysDeptDao, SysDeptEntity> implements SysDeptService {

    @Override
    @DataFilter(subDept = true, user = false, tableAlias = "t1")
    public List<SysDeptEntity> queryList(Map<String, Object> params) {
        List<SysDeptEntity> list = baseMapper.queryList(params);
        for (int i = 0; i < list.size(); i++) {
            SysDeptEntity e = list.get(i);
            if (StringUtils.isBlank(e.getParentName())) {
                e.setParentName("顶级部门");
            }
        }
        return list;
    }

    @Override
    public List<Long> queryDetpIdList(Long parentId) {
        return baseMapper.queryDetpIdList(parentId);
    }

    @Override
    public List<Long> getSubDeptIdList(Long deptId) {
        //部门及子部门ID列表
        List<Long> deptIdList = new ArrayList<>();

        //获取子部门ID
        List<Long> subIdList = queryDetpIdList(deptId);
        getDeptTreeList(subIdList, deptIdList);

        return deptIdList;
    }

    /**
     * 递归
     */
    private void getDeptTreeList(List<Long> subIdList, List<Long> deptIdList) {
        for (Long deptId : subIdList) {
            List<Long> list = queryDetpIdList(deptId);
            if (list.size() > 0) {
                getDeptTreeList(list, deptIdList);
            }

            deptIdList.add(deptId);
        }
    }
}

package cn.com.openportal.ffw.modules.sys.service.impl;

import cn.com.openportal.ffw.common.utils.Constant;
import cn.com.openportal.ffw.common.utils.DateUtils;
import cn.com.openportal.ffw.common.utils.PageUtils;
import cn.com.openportal.ffw.common.utils.QueryWrapperUtils;
import cn.com.openportal.ffw.modules.sys.query.SysLogQuery;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.com.openportal.ffw.modules.sys.dao.SysLogDao;
import cn.com.openportal.ffw.modules.sys.entity.SysLogEntity;
import cn.com.openportal.ffw.modules.sys.service.SysLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.Map;

@Service("sysLogService")
public class SysLogServiceImpl extends ServiceImpl<SysLogDao, SysLogEntity> implements SysLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper wrapper = new QueryWrapper<SysLogEntity>();
        wrapper=QueryWrapperUtils.getQueryWrapper(Constant.DO_AUTO, params, wrapper, SysLogEntity.class);

        Date date = null;
        String dateS = (String) params.get("createDate");
        if (StringUtils.isNotBlank(dateS)) {
            date = DateUtils.stringToDateTime(dateS);
        }
        if (null != date) {
            wrapper.lt("create_date", date);
        }

        IPage<SysLogEntity> page = this.page(
                new SysLogQuery().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public void remove() {
        QueryWrapper wrapper = new QueryWrapper<SysLogEntity>();
        this.remove(wrapper);
    }
}

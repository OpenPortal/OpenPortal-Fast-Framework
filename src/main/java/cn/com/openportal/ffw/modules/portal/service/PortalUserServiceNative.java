package cn.com.openportal.ffw.modules.portal.service;

import cn.com.openportal.ffw.common.utils.BaseServiceNative;
import cn.com.openportal.ffw.modules.portal.entity.PortalUser;
import cn.com.openportal.ffw.modules.portal.query.PortalUserQueryNative;

/**
 * Service接口
 *
 * @author LeeSon
 */
public interface PortalUserServiceNative extends BaseServiceNative<PortalUser, PortalUserQueryNative> {
}

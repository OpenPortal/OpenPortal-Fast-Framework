package cn.com.openportal.ffw.modules.oss.service;

import cn.com.openportal.ffw.common.utils.PageUtils;
import cn.com.openportal.ffw.modules.oss.entity.SysOssEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 文件上传
 *
 * @author LeeSon
 */
public interface SysOssService extends IService<SysOssEntity> {

	PageUtils queryPage(Map<String, Object> params);
}

package cn.com.openportal.ffw.modules.sys.service;

import cn.com.openportal.ffw.common.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.com.openportal.ffw.modules.sys.entity.SysLogEntity;

import java.util.Map;

/**
 * 系统日志
 *
 * @author LeeSon
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void remove();
}

package cn.com.openportal.ffw.modules.portal.service.impl;

import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.openportal.ffw.datasource.annotation.DataSource;
import cn.com.openportal.ffw.common.annotation.CacheAspectAnnotation;
import cn.com.openportal.ffw.common.utils.PageUtils;
import cn.com.openportal.ffw.modules.portal.entity.PortalUser;
import cn.com.openportal.ffw.modules.portal.dao.PortalUserDaoNative;
import cn.com.openportal.ffw.modules.portal.query.PortalUserQueryNative;
import cn.com.openportal.ffw.modules.portal.service.PortalUserServiceNative;

/**
 * Service实现
 * @author LeeSon
 */
//@CacheAspectAnnotation
@DataSource("module-test")
@Service
@Transactional
public class PortalUserServiceImplNative implements PortalUserServiceNative {

	private static final Log log = LogFactory.getLog(PortalUserServiceImplNative.class);

	@Resource
	PortalUserDaoNative portalUserDaoNative;

	/**
	 * 插入数据库
	 */
	@Override
	public Long addNative(PortalUser portalUser) {
		try {
			return portalUserDaoNative.addNative(portalUser);
		} catch (SQLException e) {
			log.error("Dao add error. " + e.getMessage(), e);
			throw new RuntimeException();
		}
		//return 0L;
	}

	/**
	 * 根据主键查找
	 */
	@Override
	@Transactional(readOnly = true)
//	@Cacheable(value = "PortalUser", key = "'getByKeyNative:id:'+#id",unless ="#result == null")
	public PortalUser getByKeyNative(Long id) {
		try {
			return portalUserDaoNative.getByKeyNative(id);
		} catch (SQLException e) {
			log.error("Dao getByKey error. " + e.getMessage(), e);
			throw new RuntimeException();
		}
		//return null;
	}

	/**
	 * 根据主键批量查询
	 */
	@Override
	@Transactional(readOnly = true)
	public List<PortalUser> getByKeysNative(List<Long> idList) {
		try {
			return portalUserDaoNative.getByKeysNative(idList);
		} catch (SQLException e) {
			log.error("Dao getByKeys error. " + e.getMessage(), e);
			throw new RuntimeException();
		}
		//return null;
	}

	/**
	 * 根据主键删除
	 */
	@Override
//	@CacheEvict(value = "PortalUser", allEntries = true)
	public Integer deleteByKeyNative(Long id) {
		try {
			return portalUserDaoNative.deleteByKeyNative(id);
		} catch (SQLException e) {
			log.error("Dao deleteByKey error. " + e.getMessage(), e);
			throw new RuntimeException();
		}
		//return -1;
	}

	/**
	 * 根据条件删除
	 */
	@Override
//	@CacheEvict(value = "PortalUser", allEntries = true)
	public Integer deleteByQueryNative(PortalUserQueryNative portalUserQueryNative) {
		try {
			return portalUserDaoNative.deleteByQueryNative(portalUserQueryNative);
		} catch (SQLException e) {
			log.error("Dao deleteByQuery error. " + e.getMessage(), e);
			throw new RuntimeException();
		}
		//return -1;
	}

	/**
	 * 根据主键批量删除
	 */
	@Override
//	@CacheEvict(value = "PortalUser", allEntries = true)
	public Integer deleteByKeysNative(List<Long> idList) {
		try {
			return portalUserDaoNative.deleteByKeysNative(idList);
		} catch (SQLException e) {
			log.error("Dao deleteByKeys error. " + e.getMessage(), e);
			throw new RuntimeException();
		}
		//return -1;
	}

	/**
	 * 删除所有
	 */
	@Override
//	@CacheEvict(value = "PortalUser", allEntries = true)
	public Integer deleteAllNative() {
		try {
			return portalUserDaoNative.deleteAllNative();
		} catch (SQLException e) {
			log.error("Dao deleteAll error. " + e.getMessage(), e);
			throw new RuntimeException();
		}
		//return -1;
	}

	/**
	 * 根据主键更新非空字段
	 */
	@Override
//	@CacheEvict(value = "PortalUser", key = "'getByKeyNative:id:'+#portalUser.id", allEntries = true)
	public Integer updateByKeyNative(PortalUser portalUser) {
		try {
			return portalUserDaoNative.updateByKeyNative(portalUser);
		} catch (SQLException e) {
			log.error("Dao updateByKey error. " + e.getMessage(), e);
			throw new RuntimeException();
		}
		//return -1;
	}

	/**
	 * 根据主键更新所有字段
	 */
	@Override
//	@CacheEvict(value = "PortalUser", key = "'getByKeyNative:id:'+#portalUser.id", allEntries = true)
	public Integer updateByKeyAllNative(PortalUser portalUser) {
		try {
			return portalUserDaoNative.updateByKeyAllNative(portalUser);
		} catch (SQLException e) {
			log.error("Dao updateKeyAll error. " + e.getMessage(), e);
			throw new RuntimeException();
		}
		//return -1;
	}

	/**
	 * 根据条件查询分页查询
	 */
	@Override
	@Transactional(readOnly = true)
	public PageUtils getPageNative(PortalUserQueryNative portalUserQueryNative) {
		try {
			return new PageUtils(portalUserDaoNative.getPageNative(portalUserQueryNative), portalUserDaoNative.getCountNative(portalUserQueryNative), portalUserQueryNative.getPageSize(), portalUserQueryNative.getPage());
		} catch (Exception e) {
			log.error("Dao getPage error. " + e.getMessage(), e);
			throw new RuntimeException();
		}
		//return null;
	}

	/**
	 * 根据条件查询
	 */
	@Override
	@Transactional(readOnly = true)
	public List<PortalUser> getListNative(PortalUserQueryNative portalUserQueryNative) {
		try {
			return portalUserDaoNative.getListNative(portalUserQueryNative);
		} catch (SQLException e) {
			log.error("getList error. " + e.getMessage(), e);
			throw new RuntimeException();
		}
		//return null;
	}

	/**
	 * 总条数
	 */
	@Override
	@Transactional(readOnly = true)
	public Integer getCountNative(PortalUserQueryNative portalUserQueryNative){
		try {
			return portalUserDaoNative.getCountNative(portalUserQueryNative);
		} catch (SQLException e) {
			log.error("getCount. " + e.getMessage(), e);
			throw new RuntimeException();
		}
		//return 0;
	}

}

package cn.com.openportal.ffw.modules.sys.query;

import cn.com.openportal.ffw.common.utils.BaseQuery;
import cn.com.openportal.ffw.modules.sys.entity.SysLogEntity;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @create 2020-01-08 9:47
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class SysLogQuery extends BaseQuery<SysLogEntity> {
}

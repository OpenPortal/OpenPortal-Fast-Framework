package cn.com.openportal.ffw.cache.utils;

import cn.com.openportal.ffw.cache.config.CacheType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.stereotype.Service;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.common.utils
 * @create 2020-01-16 22:01
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
@Service
public class CacheUtils {
    private static Logger logger = LoggerFactory.getLogger(CacheUtils.class);

    @Autowired
    CacheManager cacheManager;
    @Autowired
    CacheType springCacheType;
    @Autowired
    EhCacheManagerFactoryBean ehCacheManagerFactoryBean;

    public Object getCache(String cacheName, String key) {
        long beginTime = System.currentTimeMillis();
        Cache cache = getCacheNative(cacheName);
        if (null == cache) {
            return null;
        }
        Object obj = cache.get(key);
        if (null == obj) {
            return null;
        }
        SimpleValueWrapper wrapper = (SimpleValueWrapper) obj;
        if (null == wrapper) {
            return null;
        }
        Object object = wrapper.get();
        if (null != object) {
            // 缓存中获取到数据，直接返回
            logger.info("Get Cache cache=[" + cacheName + "] key=[" + key + "] ： Data >>>> " + object.toString());
            logger.info("Get Cache  >>>> Over Time：" + (System.currentTimeMillis() - beginTime) + "ms");
            return object;
        }
        return null;
    }

    public boolean putCache(String cacheName, String key, Object obj) {
        long beginTime = System.currentTimeMillis();
        if (null == obj) {
            return false;
        }
        try {
            if (null != obj) {
                Cache cache = getCacheNative(cacheName);
                if (null != cache) {
                    cache.put(key, obj);
                    logger.info("Put Cache cache=[" + cacheName + "] key=[" + key + "] ： Data >>>> " + obj.toString());
                    logger.info("Put Cache  >>>> Over Time：" + (System.currentTimeMillis() - beginTime) + "ms");
                    return true;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return false;
    }

    public void removeCacheKey(String cacheName, String key,String methodName) {
        long beginTime = System.currentTimeMillis();
        Cache cache = getCacheNative(cacheName);
        if(cache!=null){
            cache.evict(key);
            logger.info("Remove Evict Cache cache=[" + cacheName + "] method=[" + methodName + "] key=["+key+"]");
            logger.info("Remove Evict Cache  >>>> Over Time：" + (System.currentTimeMillis() - beginTime) + "ms");
        }
    }

    public void delCache(String cacheName, String methodName) {
        long beginTime = System.currentTimeMillis();
        Cache cache = getCacheNative(cacheName);
        if(cache!=null){
            cache.clear();
            logger.info("Clear Cache cache=[" + cacheName + "] method=[" + methodName + "]");
            logger.info("Clear Cache  >>>> Over Time：" + (System.currentTimeMillis() - beginTime) + "ms");
        }
    }

    private Cache getCacheNative(String cacheName) {
        Cache cache = cacheManager.getCache(cacheName);
        if (springCacheType.getType() == CacheType.CACHE_IS_EHCACHE) {
            if (cache == null) {
                synchronized (this) {
                    cache = cacheManager.getCache(cacheName);
                    if (cache == null) {
                        ehCacheManagerFactoryBean.getObject().addCacheIfAbsent(cacheName);
                        logger.info("Add EhCache Cache name : " + cacheName);
                        cache = cacheManager.getCache(cacheName);
                    }
                }
            }
            return cache;
        }
        return cache;
    }
}

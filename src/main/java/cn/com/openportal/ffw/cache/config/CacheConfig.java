package cn.com.openportal.ffw.cache.config;

import cn.com.openportal.ffw.common.exception.IgnoreExceptionCacheErrorHandler;
import cn.com.openportal.ffw.cache.multilevelcache.MultiLevelCacheManager;
import cn.com.openportal.ffw.cache.nullcache.NullCacheManager;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheManager;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.config
 * @create 2020-01-13 21:03
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
@Configuration
@EnableCaching
public class CacheConfig extends CachingConfigurerSupport {

    @Bean
    @Primary
    public CacheManager cacheManager(CacheType springCacheType, RedisCacheManager redisCacheManager, EhCacheCacheManager ehCacheCacheManager, ConcurrentMapCacheManager concurrentMapCacheManager, MultiLevelCacheManager multiLevelCacheManager, NullCacheManager nullCacheManager) {
        if (springCacheType.getType() == CacheType.CACHE_IS_Multi) {
            // SpringBoot 多级缓存实现
            return multiLevelCacheManager;
        } else if (springCacheType.getType() == CacheType.CACHE_IS_REDIS) {
            // SpringBoot Redis缓存实现
            return redisCacheManager;
        } else if (springCacheType.getType() == CacheType.CACHE_IS_EHCACHE) {
            // SpringBoot ehCache缓存实现
            return ehCacheCacheManager;
        } else if(springCacheType.getType() == CacheType.CACHE_IS_LOCALCACHE){
            // SpringBoot concurrentMap堆缓存实现
            return concurrentMapCacheManager;
        }else{
            return nullCacheManager;
        }
    }

    /**
     * 添加自定义缓存异常处理
     * 当缓存读写异常时,忽略异常
     */
    @Override
    public CacheErrorHandler errorHandler() {
        return new IgnoreExceptionCacheErrorHandler();
    }
}

package cn.com.openportal.ffw.cache.shiro;

import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.config
 * @create 2020-01-11 22:16
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
//@Configuration
//public class ShiroEhcacheConfig {
//
//    @Bean("shiroEhCacheManager")
//    public EhCacheManager shiroEhCacheManager(EhCacheManagerFactoryBean ehCacheManagerFactoryBean) {
//        EhCacheManager EhCacheManager = new EhCacheManager();
//        EhCacheManager.setCacheManager(ehCacheManagerFactoryBean.getObject());
//        return EhCacheManager;
//    }
//}

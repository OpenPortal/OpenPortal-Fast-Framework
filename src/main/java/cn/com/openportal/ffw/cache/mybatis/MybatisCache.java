package cn.com.openportal.ffw.cache.mybatis;

import cn.com.openportal.ffw.cache.config.CacheType;
import cn.com.openportal.ffw.common.utils.SpringContextUtils;
import org.apache.ibatis.cache.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.cache.support.SimpleValueWrapper;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 自定义Mybatis的二级缓存
 * 实现Mybatis的Cache接口
 *
 * @author LeeSon
 */
public class MybatisCache implements Cache {

    private static final Logger logger = LoggerFactory.getLogger(MybatisCache.class);

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);

    private CacheManager cacheManager;
    private CacheType springCacheType;
    private net.sf.ehcache.CacheManager ehCacheManager;

    private String id;

    public MybatisCache(final String id) {
        if (id == null) {
            throw new IllegalArgumentException("Mybatis Cache instances require an ID");
        }
        logger.debug("Mybatis Cache instances id : " + id);
        this.id = id;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void putObject(Object key, Object value) {
        try {
            if (value != null) {
                org.springframework.cache.Cache cache = getCacheNative(getId());
                if (null != cache) {
                    cache.put(key, value);
                    logger.debug("Mybatis Cache Put : " + getId() + " key= " + key + " value= " + value.toString());
                }
            }
        } catch (Exception e) {
            logger.error("Mybatis Cache Put Error : " + getId() + " key= " + key);
        }
    }

    @Override
    public Object getObject(Object key) {
        try {
            if (key != null) {
                org.springframework.cache.Cache cache = getCacheNative(getId());
                if (null == cache) {
                    return null;
                }
                Object obj = cache.get(key);
                if (null == obj) {
                    return null;
                }
                SimpleValueWrapper wrapper = (SimpleValueWrapper) obj;
                if (null == wrapper) {
                    return null;
                }
                Object object = wrapper.get();
                if (null != object) {
                    // 缓存中获取到数据，直接返回
                    logger.debug("Mybatis Cache Get : " + getId() + " key= " + key + " value= " + object.toString());
                    return object;
                }
                return null;
            }
        } catch (Exception e) {
            logger.error("Mybatis Cache Get Error : " + getId() + " key= " + key);
        }
        return null;
    }

    @Override
    public Object removeObject(Object key) {
        try {
            org.springframework.cache.Cache cache = getCacheNative(getId());
            if (null != cache) {
                if (key != null) {
                    Object obj = cache.get(key);
                    if (null == obj) {
                        cache.clear();
                        logger.debug("Mybatis Cache Evict Remove Obj NUll to Clear : " + getId());
                        return null;
                    }
                    SimpleValueWrapper wrapper = (SimpleValueWrapper) obj;
                    if (null == wrapper) {
                        cache.clear();
                        logger.debug("Mybatis Cache Evict Remove Wrapper NUll to Clear : " + getId());
                        return null;
                    }
                    Object object = wrapper.get();
                    if (null == object) {
                        cache.clear();
                        logger.debug("Mybatis Cache Evict Remove Value NUll to Clear : " + getId());
                        return null;
                    }else {
                        cache.evict(key);
                        logger.debug("Mybatis Cache Evict Remove : " + getId() + " key= " + key);
                        return object;
                    }
                }else{
                    cache.clear();
                    logger.debug("Mybatis Cache Evict Remove Key NUll to Clear : " + getId());
                }
            }
        } catch (Exception e) {
            logger.error("Mybatis Cache Evict Remove Error : " + getId() + " key= " + key);
        }
        return null;
    }

    @Override
    public void clear() {
        try {
            org.springframework.cache.Cache cache = getCacheNative(getId());
            if (null != cache) {
                cache.clear();
                logger.debug("Mybatis Cache Clear : " + getId());
            }
        } catch (Exception e) {
            logger.error("Mybatis Cache Clear Error : " + getId());
        }
    }

    @Override
    public int getSize() {
        Integer size = cacheManager.getCacheNames().size();
        logger.debug("Mybatis Cache Size : " + size);
        return size;
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return this.readWriteLock;
    }

    private org.springframework.cache.Cache getCacheNative(String cacheName) {
        if (cacheManager == null) {
            cacheManager = getCacheManager();
        }
        if (springCacheType == null) {
            springCacheType = getCacheType();
        }

        Object cache = cacheManager.getCache(cacheName);
        if (springCacheType.getType() == CacheType.CACHE_IS_EHCACHE) {
            if (cache == null) {
                synchronized (this) {
                    cache = cacheManager.getCache(cacheName);
                    if (cache == null) {
                        if (ehCacheManager == null) {
                            ehCacheManager = getEhCacheManager();
                        }
                        ehCacheManager.addCacheIfAbsent(cacheName);
                        logger.debug("Mybatis EhCache Add Cache name : " + cacheName);
                        cache = cacheManager.getCache(cacheName);
                    }
                }
            }
            return (org.springframework.cache.Cache) cache;
        }
        return (org.springframework.cache.Cache) cache;
    }

    private CacheManager getCacheManager() {
        if (this.cacheManager == null) {
            this.cacheManager = (CacheManager) SpringContextUtils.getBean("cacheManager");
        }
        return this.cacheManager;
    }

    private net.sf.ehcache.CacheManager getEhCacheManager() {
        if (this.ehCacheManager == null) {
//            EhCacheManagerFactoryBean ehCacheManagerFactoryBean = (EhCacheManagerFactoryBean) SpringContextUtils.getBean("&ehCacheManagerFactoryBean");
//            this.ehCacheManager = ehCacheManagerFactoryBean.getObject();
            this.ehCacheManager = (net.sf.ehcache.CacheManager) SpringContextUtils.getBean("ehCacheManagerFactoryBean");
        }
        return this.ehCacheManager;
    }

    private CacheType getCacheType() {
        if (this.springCacheType == null) {
            this.springCacheType = (CacheType) SpringContextUtils.getBean("springCacheType");
        }
        return this.springCacheType;
    }
}

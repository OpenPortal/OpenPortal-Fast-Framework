package cn.com.openportal.ffw.cache.nullcache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.multilevelcache
 * @create 2020-01-25 1:46
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class NullCacheManager implements CacheManager {
    private static Logger log = LoggerFactory.getLogger(NullCacheManager.class);

    private Set<String> cacheNames = new HashSet<>();
    private NullCache nullCache;

    public NullCacheManager() {
        synchronized (this) {
            if (null == this.nullCache) {
                this.nullCache = new NullCache();
                this.cacheNames = new HashSet<>();
            }
        }
    }

    @Override
    public Cache getCache(String name) {
        log.debug("Null Cache Get Cache Instance, Cache Name is : {}", name);
        return nullCache;
    }

    @Override
    public Collection<String> getCacheNames() {
        return this.cacheNames;
    }
}

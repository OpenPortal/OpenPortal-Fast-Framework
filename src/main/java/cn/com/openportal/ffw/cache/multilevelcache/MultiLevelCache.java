package cn.com.openportal.ffw.cache.multilevelcache;

import cn.com.openportal.ffw.cache.config.CacheRedisConfig;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.ehcache.EhCacheCache;
import org.springframework.cache.support.AbstractValueAdaptingCache;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Constructor;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.multilevelcache
 * @create 2020-01-22 2:15
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class MultiLevelCache extends AbstractValueAdaptingCache {
    private static Logger log = LoggerFactory.getLogger(MultiLevelCache.class);

    private String name;

    private RedisTemplate<String, Object> redisTemplate;

    EhCacheCache ehcacheCache;

    private String cachePrefix;

    private long defaultExpiration = 0;

    private Map<String, Long> expires;

    protected MultiLevelCache(boolean allowNullValues) {
        super(allowNullValues);
    }

    public MultiLevelCache(String name, RedisTemplate<String, Object> redisTemplate, EhCacheCache ehcacheCache, MultiLevelCacheProperties multiLevelCacheProperties) {
        super(multiLevelCacheProperties.isCacheNullValues());
        this.name = name;
        this.redisTemplate = redisTemplate;
        this.ehcacheCache = ehcacheCache;
        this.cachePrefix = multiLevelCacheProperties.getCachePrefix();
        this.defaultExpiration = multiLevelCacheProperties.getRedis().getDefaultExpiration();
        this.expires = multiLevelCacheProperties.getRedis().getExpires();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Object getNativeCache() {
        return this;
    }

    @Override
    public <T> T get(Object key, Callable<T> valueLoader) {
        Object value = lookup(key);
        if (value != null) {
            return (T) value;
        }

        ReentrantLock lock = new ReentrantLock();
        try {
            lock.lock();
            value = lookup(key);
            if (value != null) {
                return (T) value;
            }
            value = valueLoader.call();
            Object storeValue = toStoreValue(valueLoader.call());
            put(key, storeValue);
            return (T) value;
        } catch (Exception e) {
            try {
                Class<?> c = Class.forName("org.springframework.cache.Cache$ValueRetrievalException");
                Constructor<?> constructor = c.getConstructor(Object.class, Callable.class, Throwable.class);
                RuntimeException exception = (RuntimeException) constructor.newInstance(key, valueLoader, e.getCause());
                throw exception;
            } catch (Exception e1) {
                throw new IllegalStateException(e1);
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void put(Object key, Object value) {
        if (!super.isAllowNullValues() && value == null) {
            this.evict(key);
            return;
        }
        long expire = getExpire();
        if (expire > 0) {
            redisTemplate.opsForValue().set(getKey(key), toStoreValue(value), expire, TimeUnit.MINUTES);
        } else {
            redisTemplate.opsForValue().set(getKey(key), toStoreValue(value));
        }

        //通过redis推送消息，使其他服务的ehcache失效。
        //原来的有个缺点：服务1给缓存put完KV后推送给redis的消息，服务1本身也会接收到该消息，
        // 然后会将刚刚put的KV删除。这里把ehcacheCache的hashcode传过去，避免这个问题。
        push(new RedisCacheMessage(this.name, key, this.ehcacheCache.hashCode()));
        ehcacheCache.put(key, value);

        log.debug("Multi-Level-Cache Put Cache, CacheName : {}, key : {}, value : {}", name, key, value.toString());
    }

    //key的生成    name:cachePrefix:key
    private String getKey(Object key) {
        return this.name.concat(":").concat(StringUtils.isEmpty(cachePrefix) ? key.toString() : cachePrefix.concat(":").concat(key.toString()));
    }

    private long getExpire() {
        long expire = defaultExpiration;
        Long cacheNameExpire = expires.get(this.name);
        return cacheNameExpire == null ? expire : cacheNameExpire.longValue();
    }

    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        Object cacheKey = getKey(key);
        Object prevValue = null;
        // 考虑使用分布式锁，或者将redis的setIfAbsent改为原子性操作
        synchronized (key) {
            prevValue = redisTemplate.opsForValue().get(cacheKey);
            if (prevValue == null) {
                long expire = getExpire();
                if (expire > 0) {
                    redisTemplate.opsForValue().set(getKey(key), toStoreValue(value), expire, TimeUnit.MINUTES);
                } else {
                    redisTemplate.opsForValue().set(getKey(key), toStoreValue(value));
                }

                push(new RedisCacheMessage(this.name, key, this.ehcacheCache.hashCode()));

                ehcacheCache.put(key, toStoreValue(value));
            }
        }
        return toValueWrapper(prevValue);
    }

    @Override
    public void evict(Object key) {
        // 先清除redis中缓存数据，然后清除ehcache中的缓存，避免短时间内如果先清除ehcache缓存后其他请求会再从redis里加载到ehcache中
        boolean result = redisTemplate.delete(getKey(key));
        log.debug("Multi-Level-Cache Remove Evict Redis Cache, CacheName : {}, result : {}", name, result);

        push(new RedisCacheMessage(this.name, key, this.ehcacheCache.hashCode()));

        ehcacheCache.evict(key);

        log.debug("Multi-Level-Cache Remove Evict Cache, CacheName : {}, key : {}", name, key);
    }

    @Override
    public void clear() {
        // 先清除redis中缓存数据，然后清除ehcache中的缓存，避免短时间内如果先清除ehcache缓存后其他请求会再从redis里加载到ehcache中
        Set<String> keys = redisTemplate.keys(this.name.concat(":") + "*");
        if (!CollectionUtils.isEmpty(keys)) {
            Long size = redisTemplate.delete(keys);
            log.debug("Multi-Level-Cache Clear Redis Cache, CacheName : {}, Size : {}", name, size);
        }

        push(new RedisCacheMessage(this.name, null, this.ehcacheCache.hashCode()));

        ehcacheCache.clear();

        log.debug("Multi-Level-Cache Clear Cache, CacheName : {}", name);
    }

    //获根据key取缓存,如果返回null，则要读取持久层
    @Override
    protected Object lookup(Object key) {
        Object cacheKey = getKey(key);
        Object value = null;
        Cache.ValueWrapper wrapper = ehcacheCache.get(key);
        if (wrapper != null) {
            value = wrapper.get();
        }
        if (value != null) {
            log.debug("Multi-Level-Cache Get Cache from Local, CacheName : {}, key : {}", name, cacheKey);
            return value;
        }

        value = redisTemplate.opsForValue().get(cacheKey);

        if (value != null) {
            //将二级缓存重新复制到一级缓存。原理是最近访问的key很可能再次被访问
            ehcacheCache.put(key, value);
            log.debug("Multi-Level-Cache Get Cache from Redis and Put Local, CacheName : {}, Key : {}", name, cacheKey);
        }
        return value;
    }


    /**
     * 缓存变更时，利用redis的消息订阅功能，通知其他节点清理本地缓存。
     *
     * @param message
     * @description
     */
    private void push(RedisCacheMessage message) {

        redisTemplate.convertAndSend(CacheRedisConfig.REDIS_TOPIC, message);

        log.debug("Multi-Level-Cache Push Redis Topic Message, message : {}", message.toString());
    }

    /**
     * @param key
     * @description 清理本地缓存
     */
    public void clearLocal(Object key) {
        if (key == null) {
            ehcacheCache.clear();
        } else {
            ehcacheCache.evict(key);
        }
        log.debug("Multi-Level-Cache Clear Local Cache, CacheName : {}, key : {}", name, key);
    }

    public Cache getLocalCache() {
        return ehcacheCache;
    }
}

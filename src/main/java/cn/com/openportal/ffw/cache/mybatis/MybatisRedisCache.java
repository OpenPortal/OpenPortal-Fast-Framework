package cn.com.openportal.ffw.cache.mybatis;

import cn.com.openportal.ffw.common.utils.SpringContextUtils;
import org.apache.ibatis.cache.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 使用Redis来做Mybatis的二级缓存
 * 实现Mybatis的Cache接口
 *
 * @author LeeSon
 */
public class MybatisRedisCache implements Cache {

    private static final Logger logger = LoggerFactory.getLogger(MybatisRedisCache.class);
    /**
     * 默认过期时长，单位：秒
     */
    public final static long DEFAULT_EXPIRE = 60 * 60 * 24;

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);

    @Autowired
    private RedisTemplate redisTemplate;

    private String id;

    public MybatisRedisCache(final String id) {
        if (id == null) {
            throw new IllegalArgumentException("Mybatis Redis Cache instances require an ID");
        }
        logger.debug("Mybatis Redis Cache instances id : " + id);
        this.id = id;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void putObject(Object key, Object value) {
        try {
            if (value != null) {
                if (this.redisTemplate == null) {
                    this.redisTemplate = getRedisTemplate();
                }
                this.redisTemplate.opsForValue().set(key.toString(), value, 2, TimeUnit.DAYS);
                this.redisTemplate.expire(key.toString(), DEFAULT_EXPIRE, TimeUnit.SECONDS);
                logger.debug("Mybatis Redis Cache Put : " + getId() + " key= " + key + " value= " + value.toString());
            }
        } catch (Exception e) {
            logger.error("Mybatis Redis Cache Put Error : " + getId() + " key= " + key);
        }
    }

    @Override
    public Object getObject(Object key) {
        try {
            if (key != null) {
                if (this.redisTemplate == null) {
                    this.redisTemplate = getRedisTemplate();
                }
                Object obj = this.redisTemplate.opsForValue().get(key.toString());
                this.redisTemplate.expire(key.toString(), DEFAULT_EXPIRE, TimeUnit.SECONDS);
                logger.debug("Mybatis Redis Cache Get : " + getId() + " key= " + key + " value= " + obj.toString());
                return obj;
            }
        } catch (Exception e) {
            logger.error("Mybatis Redis Cache Get Error : " + getId() + " key= " + key);
        }
        return null;
    }

    @Override
    public Object removeObject(Object key) {
        try {
            if (key != null) {
                if (this.redisTemplate == null) {
                    this.redisTemplate = getRedisTemplate();
                }
                this.redisTemplate.delete(key.toString());
                logger.debug("Mybatis Redis Cache Remove: " + getId() + " key= " + key);
            }
        } catch (Exception e) {
            logger.error("Mybatis Redis Cache Remove Error : " + getId() + " key= " + key);
        }
        return null;
    }

    @Override
    public void clear() {
        try {
            if (this.redisTemplate == null) {
                this.redisTemplate = getRedisTemplate();
            }
            Set<String> keys = this.redisTemplate.keys("*:" + this.id + "*");
            if (!CollectionUtils.isEmpty(keys)) {
                this.redisTemplate.delete(keys);
                logger.debug("Mybatis Redis Cache Clear : " + getId());
            }
        } catch (Exception e) {
            logger.error("Mybatis Redis Cache Clear Error : " + getId());
        }
    }

    @Override
    public int getSize() {
        if (this.redisTemplate == null) {
            this.redisTemplate = getRedisTemplate();
        }
        Long size = (Long) this.redisTemplate.execute(new RedisCallback<Long>() {
            @Override
            public Long doInRedis(RedisConnection connection) throws DataAccessException {
                return connection.dbSize();
            }
        });
        logger.debug("Mybatis Redis Cache Size : " + size);
        return size.intValue();
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return this.readWriteLock;
    }

    private RedisTemplate getRedisTemplate() {
        if (this.redisTemplate == null) {
            this.redisTemplate = (RedisTemplate) SpringContextUtils.getBean("redisTemplate");
        }
        return this.redisTemplate;
    }
}

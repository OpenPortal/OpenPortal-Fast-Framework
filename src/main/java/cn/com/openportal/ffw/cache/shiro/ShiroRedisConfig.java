package cn.com.openportal.ffw.cache.shiro;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.config
 * @create 2020-01-11 21:56
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
//@Configuration
//public class ShiroRedisConfig {
//    @Value("${spring.redis.host}")
//    private String host;
//    @Value("${spring.redis.port}")
//    private int port;
//    @Value("${spring.redis.password}")
//    private String password;
//    @Value("${spring.redis.database}")
//    private int database;
//    @Value("${spring.redis.timeout}")
//    private String timeout;
//
//    @Bean
//    public RedisManager redisManager() {
//        RedisManager redisManager = new RedisManager();
//        redisManager.setHost(host);
//        redisManager.setPort(port);
//        redisManager.setPassword(password);
//        redisManager.setDatabase(database);
//        if (StringUtils.isNotBlank(timeout)) {
//            try {
//                redisManager.setTimeout(Integer.valueOf(timeout.replaceAll("ms", "")));
//            } catch (Exception e) {
//            }
//        }
//        return redisManager;
//    }
//
//    @Bean("shiroRedisCacheManager")
//    public RedisCacheManager shiroRedisCacheManager(RedisManager redisManager) {
//        RedisCacheManager redisCacheManager = new RedisCacheManager();
//        redisCacheManager.setRedisManager(redisManager);
//        redisCacheManager.setExpire(5000);
//        //指定存入Redis的主键
//        redisCacheManager.setPrincipalIdFieldName("userId");
//        return redisCacheManager;
//    }
//
//    @Bean
//    public SessionManager sessionManager(RedisManager redisManager) {
//        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
//        sessionManager.setSessionDAO(redisSessionDAO(redisManager));
//        return sessionManager;
//    }
//
//    @Bean
//    public RedisSessionDAO redisSessionDAO(RedisManager redisManager) {
//        RedisSessionDAO redisSessionDAO = new RedisSessionDAO();
//        redisSessionDAO.setRedisManager(redisManager);
//        redisSessionDAO.setExpire(2000);
//        return redisSessionDAO;
//    }
//}

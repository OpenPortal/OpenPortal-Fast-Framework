package cn.com.openportal.ffw.cache.nullcache;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.multilevelcache.nullcache
 * @create 2020-01-25 2:08
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
@Configuration
public class NullCacheManagerConfig {

    @Bean
    public NullCacheManager nullCacheManager(){
        return new NullCacheManager();
    }
}

package cn.com.openportal.ffw.cache.shiro;

import cn.com.openportal.ffw.cache.config.CacheType;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.web.session.mgt.ServletContainerSessionManager;
import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.config
 * @create 2020-01-23 0:41
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
@Configuration
public class ShiroCacheManagerConfig {
    @Bean
    public ShiroCacheManager shiroCacheManager(CacheType springCacheType, CacheManager cacheManager, EhCacheCacheManager ehCacheCacheManager) {
        return new ShiroCacheManager(springCacheType, cacheManager, ehCacheCacheManager);
    }

    /**
     * spring session管理器（多机环境）
     */
    @Bean
    public SessionManager shiroSessionManager() {
        return new ServletContainerSessionManager();
    }
}

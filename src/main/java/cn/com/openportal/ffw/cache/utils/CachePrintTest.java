package cn.com.openportal.ffw.cache.utils;

import cn.com.openportal.ffw.common.utils.SpringContextUtils;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheCache;
import org.springframework.cache.ehcache.EhCacheCacheManager;

import java.util.Collection;
import java.util.Set;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.common.utils
 * @create 2020-01-14 0:07
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class CachePrintTest {
    private static final Logger logger = LoggerFactory.getLogger(CachePrintTest.class);

    public static void getCache(){

        try {

            System.out.println("Spring Cache #################################################################");
            try {
                CacheManager cacheManager = (CacheManager) SpringContextUtils
                        .getBean("cacheManager");
                Collection<String> cacheNames=cacheManager.getCacheNames();
                for (String cacheName:cacheNames) {
                    System.out.println("====================================================");
                    System.out.println(cacheName);
                    org.springframework.cache.Cache cache=cacheManager.getCache(cacheName);
                    System.out.println(cache.toString());
                    org.springframework.cache.Cache.ValueWrapper valueWrapper=cache.get("getByKeyNative:id:1");
                    Object obj=valueWrapper.get();
                    System.out.println(obj.toString());
                    System.out.println("====================================================");
                }
            }catch (Exception e){
                logger.error(e.getMessage(), e);
            }
            System.out.println();


            System.out.println("ehCache Cache #################################################################");
            try {
                EhCacheCacheManager ehCacheCacheManager = (EhCacheCacheManager) SpringContextUtils
                        .getBean("ehCacheCacheManager");
                Collection<String> cacheNames=ehCacheCacheManager.getCacheNames();
                for (String cacheName:cacheNames) {
                    System.out.println("====================================================");
                    System.out.println(cacheName);
                    org.springframework.cache.ehcache.EhCacheCache cache= (EhCacheCache) ehCacheCacheManager.getCache(cacheName);
                    System.out.println(cache.toString());
                    System.out.println("====================================================");
                }
            }catch (Exception e){
                logger.error(e.getMessage(), e);
            }
            System.out.println();
            System.out.println();
            System.out.println();
            System.out.println();


            System.out.println("Shiro EhCache Cache #################################################################");
            try {
                EhCacheManager ehCacheManager = (EhCacheManager) SpringContextUtils
                        .getBean("shiroEhCacheManager");
                net.sf.ehcache.CacheManager cacheManager=ehCacheManager.getCacheManager();
                String[] cacheNames=cacheManager.getCacheNames();
                for (String cacheName:cacheNames) {
                    System.out.println("====================================================");
                    System.out.println(cacheName);
                    org.apache.shiro.cache.Cache cache=ehCacheManager.getCache(cacheName);
                    Set keys=cache.keys();
                    for (Object key:keys){
                        System.out.println("++++++++++++"+cacheName+"++++++++++++++++<<<<");
                        System.out.println(key.getClass());
                        System.out.println(key.toString());
                        System.out.println(":::::::::");
                        System.out.println(cache.get(key).getClass());
                        System.out.println(cache.get(key).toString());
                        System.out.println("++++++++++++"+cacheName+"++++++++++++++++>>>>");
                    }
                    System.out.println("====================================================");
                }
            }catch (Exception e){
                logger.error(e.getMessage(), e);
            }
            System.out.println();



        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}

package cn.com.openportal.ffw.cache.shiro;

import cn.com.openportal.ffw.cache.config.CacheType;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.util.Destroyable;
import org.springframework.cache.ehcache.EhCacheCacheManager;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.config
 * @create 2020-01-23 0:52
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class ShiroCacheManager implements CacheManager, Destroyable {

    private static final String CACHE_NAME_PREFIX = "Shiro:";

    private ConcurrentMap<String, Cache> cacheMap = new ConcurrentHashMap<String, Cache>();

    private org.springframework.cache.CacheManager cacheManager;
    private CacheType springCacheType;
    private EhCacheCacheManager ehCacheCacheManager;

    public ShiroCacheManager(CacheType springCacheType, org.springframework.cache.CacheManager cacheManager, EhCacheCacheManager ehCacheCacheManager) {
        this.springCacheType = springCacheType;
        this.cacheManager = cacheManager;
        this.ehCacheCacheManager = ehCacheCacheManager;
    }

    @Override
    public void destroy() throws Exception {
        cacheManager = null;
    }

    @Override
    public <K, V> Cache<K, V> getCache(String name) {
        if (name == null) {
            return null;
        }
        name = CACHE_NAME_PREFIX + name;
        Cache cache = cacheMap.get(name);
        if (cache != null) {
            return cache;
        }
        afterIfEhcache(name);
        cache = new ShiroCache<K, V>(name, cacheManager);
        Cache oldCache = cacheMap.putIfAbsent(name, cache);
        return oldCache == null ? cache : oldCache;
    }

    public void afterIfEhcache(String name) {
        if (springCacheType.getType() == CacheType.CACHE_IS_EHCACHE) {
            // SpringBoot ehCache缓存实现
            ehCacheCacheManager.getCacheManager().addCacheIfAbsent(name);
        }
    }
}

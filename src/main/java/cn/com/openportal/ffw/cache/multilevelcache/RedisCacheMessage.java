package cn.com.openportal.ffw.cache.multilevelcache;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.multilevelcache
 * @create 2020-01-21 22:54
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
@Data
public class RedisCacheMessage implements Serializable {

    private static final long serialVersionUID = 5987219310442078193L;

    private String cacheName;

    private Object key;

    private Integer sender;

    public RedisCacheMessage(String cacheName, Object key) {
        super();
        this.cacheName = cacheName;
        this.key = key;
    }

    public RedisCacheMessage(String cacheName, Object key, Integer sender) {
        super();
        this.cacheName = cacheName;
        this.key = key;
        this.sender = sender;
    }

}

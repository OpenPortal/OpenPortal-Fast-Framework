package cn.com.openportal.ffw.cache.shiro;

import org.apache.shiro.cache.CacheException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

import java.util.Collection;
import java.util.Set;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.config
 * @create 2020-01-23 0:54
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class ShiroCache<K, V> implements org.apache.shiro.cache.Cache<K, V> {
    private static final Logger log = LoggerFactory.getLogger(ShiroCache.class);
    private CacheManager cacheManager;
    private Cache cache;

    public ShiroCache(String name, CacheManager cacheManager) {
        if (name == null || cacheManager == null) {
            throw new IllegalArgumentException("Shiro CacheManager or CacheName Null");
        }
        this.cacheManager = cacheManager;
        //这里首先是从父类中获取这个cache,如果没有会创建一个redisCache,初始化这个redisCache的时候
        //会设置它的过期时间如果没有配置过这个缓存的，那么默认的缓存时间是为0的，如果配置了，就会把配置的时间赋予给这个RedisCache
        //如果从缓存的过期时间为0，就表示这个RedisCache不存在了，这个redisCache实现了spring中的cache
        this.cache = cacheManager.getCache(name);
    }

    @Override
    public V get(K key) throws CacheException {
        if (key == null) {
            return null;
        }
        Cache.ValueWrapper valueWrapper = cache.get(key);
        if (valueWrapper == null) {
            return null;
        }
        log.info("Shiro Get key {}, value {}", key, (V) valueWrapper.get().toString());
        return (V) valueWrapper.get();
    }

    @Override
    public V put(K key, V value) throws CacheException {
        cache.put(key, value);
        log.info("Shiro Put key {}, value {}", key, value.toString());
        return get(key);
    }

    @Override
    public V remove(K key) throws CacheException {
        V v = get(key);
        cache.evict(key);
        log.info("Shiro Remove key {}, value {}", key, v.toString());
        return v;
    }

    @Override
    public void clear() throws CacheException {
        cache.clear();
        log.info("Shiro Clear");
    }

    @Override
    public int size() {
        return cacheManager.getCacheNames().size();
    }

    /**
     * 获取缓存中所的key值
     */
    @Override
    public Set<K> keys() {
        return (Set<K>) cacheManager.getCacheNames();
    }

    /**
     * 获取缓存中所有的values值
     */
    @Override
    public Collection<V> values() {
        return (Collection<V>) cache.get(cacheManager.getCacheNames()).get();
    }

    @Override
    public String toString() {
        return "Shiro Cache : [" + cache + "]";
    }
}

package cn.com.openportal.ffw.cache.multilevelcache;

import cn.com.openportal.ffw.cache.config.CacheRedisConfig;
import cn.com.openportal.ffw.cache.config.CacheType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.multilevelcache
 * @create 2020-01-21 23:21
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
@Configuration
public class RedisCacheMessageListenerConfig {
    @Bean
    public RedisMessageListenerContainer redisMessageListenerContainer(CacheType springCacheType, RedisTemplate<String, Object> redisTemplate,
                                                                       MultiLevelCacheManager multiLevelCacheManager) {
        RedisMessageListenerContainer redisMessageListenerContainer = new RedisMessageListenerContainer();
        if (springCacheType.getType() == CacheType.CACHE_IS_Multi) {
            redisMessageListenerContainer.setConnectionFactory(redisTemplate.getConnectionFactory());
            RedisCacheMessageListener redisCacheMessageListener = new RedisCacheMessageListener(redisTemplate, multiLevelCacheManager);
            redisMessageListenerContainer.addMessageListener(redisCacheMessageListener, new ChannelTopic(CacheRedisConfig.REDIS_TOPIC));
        }
        return redisMessageListenerContainer;
    }
}

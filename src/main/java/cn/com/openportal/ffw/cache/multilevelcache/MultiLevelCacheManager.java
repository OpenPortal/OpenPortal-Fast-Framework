package cn.com.openportal.ffw.cache.multilevelcache;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import cn.com.openportal.ffw.cache.config.CacheType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheCache;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.multilevelcache
 * @create 2020-01-22 3:00
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class MultiLevelCacheManager implements CacheManager {
    private static Logger log = LoggerFactory.getLogger(MultiLevelCacheManager.class);

    private ConcurrentMap<String, Cache> cacheMap = new ConcurrentHashMap<String, Cache>();

    private MultiLevelCacheProperties multiLevelCacheProperties;

    private RedisTemplate<String, Object> redisTemplate;

    private boolean dynamic = true;

    private Set<String> cacheNames;

    private EhCacheCacheManager ehCacheCacheManager;

    public MultiLevelCacheManager(CacheType springCacheType, MultiLevelCacheProperties multiLevelCacheProperties,
                                  RedisTemplate<String, Object> redisTemplate, EhCacheCacheManager ehCacheCacheManager) {
        super();
        this.multiLevelCacheProperties = multiLevelCacheProperties;
        this.redisTemplate = redisTemplate;
        this.ehCacheCacheManager = ehCacheCacheManager;
        this.dynamic = multiLevelCacheProperties.isDynamic();
        if (springCacheType.getType() == CacheType.CACHE_IS_Multi) {
            this.cacheNames = multiLevelCacheProperties.getCacheNames();
        } else {
            this.cacheNames = new HashSet<>();
        }
    }

    @Override
    public Cache getCache(String name) {
        Cache cache = cacheMap.get(name);
        if (cache != null) {
            return cache;
        }
        if (!dynamic && !cacheNames.contains(name)) {
            return cache;
        }

        cache = new MultiLevelCache(name, redisTemplate, getEhcache(name), multiLevelCacheProperties);
        Cache oldCache = cacheMap.putIfAbsent(name, cache);
        cacheNames.add(name);
        log.debug("Multi-Level-Cache Create Cache Instance, Cache Name is : {}", name);
        return oldCache == null ? cache : oldCache;
    }

    public EhCacheCache getEhcache(String name) {
        EhCacheCache res = (EhCacheCache) ehCacheCacheManager.getCache(name);
        if (res != null) {
            return res;
        }
        ehCacheCacheManager.getCacheManager().addCacheIfAbsent(name);
        return (EhCacheCache) ehCacheCacheManager.getCache(name);
    }

    @Override
    public Collection<String> getCacheNames() {
        return this.cacheNames;
    }

    public void clearLocal(String cacheName, Object key, Integer sender) {
        Cache cache = cacheMap.get(cacheName);
        if (cache == null) {
            return;
        }

        MultiLevelCache multiLevelCache = (MultiLevelCache) cache;
        //如果是发送者本身发送的消息，就不进行key的清除
        Cache localCache = multiLevelCache.getLocalCache();
        if (null == localCache) {
            return;
        }
        if (localCache.hashCode() != sender) {
            multiLevelCache.clearLocal(key);
        }
    }
}

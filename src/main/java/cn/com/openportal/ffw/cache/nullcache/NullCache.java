package cn.com.openportal.ffw.cache.nullcache;

import org.springframework.cache.support.AbstractValueAdaptingCache;

import java.lang.reflect.Constructor;
import java.util.concurrent.Callable;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.multilevelcache.nullcache
 * @create 2020-01-25 1:48
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class NullCache extends AbstractValueAdaptingCache {

    protected NullCache(boolean allowNullValues) {
        super(allowNullValues);
    }

    public NullCache() {
        super(false);
    }

    @Override
    protected Object lookup(Object o) {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public Object getNativeCache() {
        return this;
    }

    @Override
    public <T> T get(Object key, Callable<T> callable) {
        try {
            Object value = callable.call();
            return (T) value;
        } catch (Exception e) {
            try {
                Class<?> c = Class.forName("org.springframework.cache.Cache$ValueRetrievalException");
                Constructor<?> constructor = c.getConstructor(Object.class, Callable.class, Throwable.class);
                RuntimeException exception = (RuntimeException) constructor.newInstance(key, callable, e.getCause());
                throw exception;
            } catch (Exception e1) {
                throw new IllegalStateException(e1);
            }
        }
    }

    @Override
    public void put(Object key, Object value) {
    }

    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        return toValueWrapper(null);
    }

    @Override
    public void evict(Object key) {
    }

    @Override
    public void clear() {
    }
}

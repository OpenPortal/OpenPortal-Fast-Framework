package cn.com.openportal.ffw.cache.shiro;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.config
 * @create 2020-01-11 21:56
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
//@Configuration
//public class CacheTypeShiroConfig {
//    @Value("${spring.redis.open: false}")
//    private boolean open;
//    @Value("${ffw.shiro.cache}")
//    private int cache;
//
//    @Bean
//    public CacheType shiroCacheType() {
//        CacheType type = CacheType.getCacheType();
//        try {
//            if (cache == CacheType.CACHE_IS_REDIS) {
//                if (open) {
//                    type.setType(CacheType.CACHE_IS_REDIS);
//                } else {
//                    type.setType(CacheType.CACHE_IS_NULL);
//                }
//            } else if (cache == CacheType.CACHE_IS_EHCACHE) {
//                type.setType(CacheType.CACHE_IS_EHCACHE);
//            } else if (cache == CacheType.CACHE_IS_LOCALCACHE) {
//                type.setType(CacheType.CACHE_IS_LOCALCACHE);
//            } else {
//                type.setType(CacheType.CACHE_IS_NULL);
//            }
//        } catch (Exception e) {
//        }
//        return type;
//    }
//}

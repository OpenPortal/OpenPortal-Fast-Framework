package cn.com.openportal.ffw.cache.config;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.config
 * @create 2020-01-11 22:45
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class CacheType {
    public final static int CACHE_IS_Multi = 3;
    public final static int CACHE_IS_REDIS = 2;
    public final static int CACHE_IS_EHCACHE = 1;
    public final static int CACHE_IS_LOCALCACHE = 0;
    public final static int CACHE_IS_NULL = -1;

    private int type = 0;

    private CacheType() {
    }

    public static CacheType getCacheType() {
        return new CacheType();
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}

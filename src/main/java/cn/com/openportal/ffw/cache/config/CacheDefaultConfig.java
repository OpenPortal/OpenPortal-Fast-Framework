package cn.com.openportal.ffw.cache.config;

import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.config
 * @create 2020-01-13 20:26
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
@Configuration
public class CacheDefaultConfig {

    @Bean
    public ConcurrentMapCacheManager concurrentMapCacheManager() {
        return new ConcurrentMapCacheManager();
    }
}

package cn.com.openportal.ffw.cache.multilevelcache;

import cn.com.openportal.ffw.cache.config.CacheType;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.multilevelcache
 * @create 2020-01-22 3:49
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
@Configuration
public class MultiLevelCacheManagerConfig {

    @Bean
    public MultiLevelCacheManager multiLevelCacheManager(CacheType springCacheType, MultiLevelCacheProperties multiLevelCacheProperties,
                                                         RedisTemplate<String, Object> redisTemplate, EhCacheCacheManager ehCacheCacheManager) {
        return new MultiLevelCacheManager(springCacheType, multiLevelCacheProperties, redisTemplate, ehCacheCacheManager);
    }

}

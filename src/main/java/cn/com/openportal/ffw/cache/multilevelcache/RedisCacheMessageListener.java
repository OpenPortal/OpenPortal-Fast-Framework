package cn.com.openportal.ffw.cache.multilevelcache;

import cn.com.openportal.ffw.cache.config.CacheRedisConfig;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.multilevelcache
 * @create 2020-01-21 22:55
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class RedisCacheMessageListener implements MessageListener {
    private static Logger logger = LoggerFactory.getLogger(RedisCacheMessageListener.class);
    private final static Gson gson = new Gson();

    private RedisTemplate<String, Object> redisTemplate;
    private MultiLevelCacheManager multiLevelCacheManager;

    public RedisCacheMessageListener(RedisTemplate<String, Object> redisTemplate, MultiLevelCacheManager multiLevelCacheManager) {
        super();
        this.redisTemplate = redisTemplate;
        this.multiLevelCacheManager = multiLevelCacheManager;
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        RedisCacheMessage cacheMessage = getCacheMessageFromByte(message);
        if (CacheRedisConfig.isFromJson) {
            cacheMessage = getCacheMessageFromJson(message);
        }
        if (null != cacheMessage) {
            logger.debug("Multi-Level-Cache Recevice Redis Topic Message, Clear Local Cache, CacheName {}, Key {}, Sender {}", cacheMessage.getCacheName(), cacheMessage.getKey(), cacheMessage.getSender());
            multiLevelCacheManager.clearLocal(cacheMessage.getCacheName(), cacheMessage.getKey(), cacheMessage.getSender());
        }
    }

    private RedisCacheMessage getCacheMessageFromByte(Message message) {
        return (RedisCacheMessage) redisTemplate.getValueSerializer().deserialize(message.getBody());
    }

    private RedisCacheMessage getCacheMessageFromJson(Message message) {
        String valueJson = message.toString();
        valueJson = valueJson.substring(1, valueJson.length() - 1).replaceAll("\\\\", "");
        return fromJson(valueJson, RedisCacheMessage.class);
    }

    public <T> T get(String value, Class<T> clazz) {
        return value == null ? null : fromJson(value, clazz);
    }

    /**
     * JSON数据，转成Object
     */
    private <T> T fromJson(String json, Class<T> clazz) {
        return gson.fromJson(json, clazz);
    }

}

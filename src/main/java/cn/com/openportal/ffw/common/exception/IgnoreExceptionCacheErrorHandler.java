package cn.com.openportal.ffw.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.interceptor.CacheErrorHandler;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.common.exception
 * @create 2020-01-13 22:07
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class IgnoreExceptionCacheErrorHandler implements CacheErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(IgnoreExceptionCacheErrorHandler.class);

    /**
     * 当缓存读写异常时,忽略异常
     */

    @Override
    public void handleCacheGetError(RuntimeException exception, Cache cache, Object key) {
        logger.error(exception.getMessage(), exception);
    }

    @Override
    public void handleCachePutError(RuntimeException exception, Cache cache, Object key, Object value) {
        logger.error(exception.getMessage(), exception);
    }

    @Override
    public void handleCacheEvictError(RuntimeException exception, Cache cache, Object key) {
        logger.error(exception.getMessage(), exception);
    }

    @Override
    public void handleCacheClearError(RuntimeException exception, Cache cache) {
        logger.error(exception.getMessage(), exception);
    }
}

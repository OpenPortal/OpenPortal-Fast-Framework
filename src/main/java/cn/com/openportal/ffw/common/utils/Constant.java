package cn.com.openportal.ffw.common.utils;

import cn.com.openportal.ffw.modules.sys.entity.SysUserEntity;
import cn.com.openportal.ffw.modules.sys.entity.SysUserTokenEntity;

/**
 * 常量
 *
 * @author LeeSon
 */
public class Constant {
	/** 超级管理员ID */
	public static final int SUPER_ADMIN = 1;
    /** 数据权限过滤 */
    public static final String SQL_FILTER = "sql_filter";
    public static final String SYSUSERTOKENENTITY = SysUserTokenEntity.class.getName();
    public static final String SYSUSERENTITY = SysUserEntity.class.getName();
    /**
     * 当前页码
     */
    public static final String PAGE = "page";
    /**
     * 每页显示记录数
     */
    public static final String LIMIT = "limit";
    /**
     * 排序字段
     */
    public static final String ORDER_FIELD = "sidx";
    /**
     * 排序方式
     */
    public static final String ORDER = "order";
    /**
     * 排序方式
     */
    public static final String ORDER_BY = "_order_by";
    /**
     *  升序
     */
    public static final String ASC = "asc";
    /**
     * 是否like参数
     */
    public static final String IS_LIKE = "Like";
    /**
     * 是否like比较
     */
    public static final String IS_DO_LIKE = "1";

    /**
     * Like
     */
    public static final int DO_LIKE = 2;
    /**
     * EQ
     */
    public static final int DO_EQ = 1;
    /**
     * 自动判断
     */
    public static final int DO_AUTO = 0;

    /**
     * 是否String
     */
    public static final String IS_STRING = "class java.lang.String";
    /**
     * 是否Long
     */
    public static final String IS_LONG_CLASS = "class java.lang.Long";
    /**
     * 是否long
     */
    public static final String IS_LONG = "long";
    /**
     * 是否Integer
     */
    public static final String IS_INT_CLASS = "class java.lang.Integer";
    /**
     * 是否int
     */
    public static final String IS_INT = "int";
    /**
     * 是否Boolean
     */
    public static final String IS_BOOLEAN_CLASS = "class java.lang.Boolean";
    /**
     * 是否boolean
     */
    public static final String IS_BOOLEAN = "boolean";

	/**
	 * 菜单类型
	 *
	 * @author LeeSon
	 */
    public enum MenuType {
        /**
         * 目录
         */
    	CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 定时任务状态
     *
     * @author LeeSon
     */
    public enum ScheduleStatus {
        /**
         * 正常
         */
    	NORMAL(0),
        /**
         * 暂停
         */
    	PAUSE(1);

        private int value;

        ScheduleStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 云服务商
     */
    public enum CloudService {
        /**
         * 七牛云
         */
        QINIU(1),
        /**
         * 阿里云
         */
        ALIYUN(2),
        /**
         * 腾讯云
         */
        QCLOUD(3);

        private int value;

        CloudService(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

}

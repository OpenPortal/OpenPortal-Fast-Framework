package cn.com.openportal.ffw.common.utils;

/**
 * Redis所有Keys
 *
 * @author LeeSon
 */
public class RedisKeys {

    public static String getSysConfigKey(String key){
        return "sys:config:" + key;
    }
}

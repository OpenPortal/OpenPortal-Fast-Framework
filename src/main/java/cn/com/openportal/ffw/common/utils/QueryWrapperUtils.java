package cn.com.openportal.ffw.common.utils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @create 2020-01-08 6:13
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public class QueryWrapperUtils {
    private static Logger logger = LoggerFactory.getLogger(QueryWrapperUtils.class);

    private static boolean isCanEQ(String fieldType) {
        if (fieldType.equals(Constant.IS_STRING)
                || fieldType.equals(Constant.IS_LONG_CLASS)
                || fieldType.equals(Constant.IS_LONG)
                || fieldType.equals(Constant.IS_INT_CLASS)
                || fieldType.equals(Constant.IS_INT)
                || fieldType.equals(Constant.IS_BOOLEAN_CLASS)
                || fieldType.equals(Constant.IS_BOOLEAN)
        ) {
            return true;
        }
        return false;
    }

    private static boolean isCanLike(String fieldType, String fieldParamLike) {
        if (StringUtils.isNotBlank(fieldParamLike) && Constant.IS_DO_LIKE.equals(fieldParamLike) && fieldType.equals(Constant.IS_STRING)) {
            return true;
        }
        return false;
    }

    public static QueryWrapper getQueryWrapper(int method, Map<String, Object> params, QueryWrapper wrapper, Class<?> clazz) {
        try {
            Field[] fs = clazz.getDeclaredFields();
            for (int i = 0; i < fs.length; i++) {
                String fieldName = fs[i].getName();
                if (StringUtils.isBlank(fieldName)) {
                    continue;
                }
                String fieldParam = (String) params.get(fieldName);
                if (StringUtils.isBlank(fieldParam)) {
                    continue;
                }
                String fieldType = fs[i].getGenericType().toString();
                if (StringUtils.isBlank(fieldType)) {
                    continue;
                }

                if (Constant.DO_LIKE == method) {
                    wrapper = getQueryWrapperLike(wrapper, fieldName, fieldType, fieldParam);
                    continue;
                }
                if (Constant.DO_EQ == method) {
                    wrapper = getQueryWrapperEQ(wrapper, fieldName, fieldType, fieldParam);
                    continue;
                }
                if (Constant.DO_AUTO == method) {
                    wrapper = getQueryWrapperAuto(wrapper, fieldName, fieldType, fieldParam, params);
                    continue;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return wrapper;
    }

    private static QueryWrapper getQueryWrapperAuto(QueryWrapper wrapper, String fieldName, String fieldType, String fieldParam, Map<String, Object> params) {
        try {
            if (!isCanEQ(fieldType)) {
                return wrapper;
            }
            String fieldParamLike = (String) params.get(fieldName + Constant.IS_LIKE);
            if (isCanLike(fieldType, fieldParamLike)) {
                wrapper.like(fieldName, fieldParam);
                return wrapper;
            }
            wrapper.eq(fieldName, fieldParam);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return wrapper;
    }

    private static QueryWrapper getQueryWrapperEQ(QueryWrapper wrapper, String fieldName, String fieldType, String fieldParam) {
        try {
            if (isCanEQ(fieldType)) {
                wrapper.eq(fieldName, fieldParam);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return wrapper;
    }

    private static QueryWrapper getQueryWrapperLike(QueryWrapper wrapper, String fieldName, String fieldType, String fieldParam) {
        try {
            if (isCanLike(fieldType, Constant.IS_LIKE)) {
                wrapper.like(fieldName, fieldParam);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return wrapper;
    }
}

package cn.com.openportal.ffw.common.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * BaseQuery
 *
 * @author LeeSon  QQ:25901875
 */
public class BaseQueryNative implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final int DEFAULT_SIZE = 10;
    private static final int DEFAULT_PAGE = 1;

    @ApiModelProperty(value = "当前页 默认1")
    protected int page = DEFAULT_PAGE;

    @ApiModelProperty(value = "每页条数 默认10 同limit")
    protected int pageSize = DEFAULT_SIZE;

    @ApiModelProperty(value = "每页条数 默认10 同pageSize")
    protected int limit = DEFAULT_SIZE;

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
        setPageSize(limit);
    }

    @ApiModelProperty(hidden = true) //swagger不显示
    @JsonIgnore //不转换
    protected int startRow;

    @ApiModelProperty(hidden = true) //swagger不显示
    @JsonIgnore //不转换
    protected int endRow;

    @ApiModelProperty(value = "获取字段 默认全部")
    protected String fields;

    @ApiModelProperty(value = "开始时间 依赖业务")
    protected Date begin_time;
    @ApiModelProperty(value = "结束时间 依赖业务")
    protected Date end_time;
    @ApiModelProperty(value = "开始时间1 依赖业务")
    protected Date begin_time1;
    @ApiModelProperty(value = "结束时间1 依赖业务")
    protected Date end_time1;

    @ApiModelProperty(hidden = true) //swagger不显示
    @JsonIgnore //不转换
    protected List<Long> gidList;

    @ApiModelProperty(hidden = true) //swagger不显示
    @JsonIgnore //不转换
    protected String sql_filter;

    public String getSql_filter() {
        return sql_filter;
    }

    public void sql_filter(String sql_filter) {
        this.sql_filter = sql_filter;
    }

    public List<Long> getGidList() {
        return gidList;
    }

    public void setGidList(List<Long> gidList) {
        this.gidList = gidList;
    }

    public Date getBegin_time() {
        return begin_time;
    }

    public void setBegin_time(Date begin_time) {
        this.begin_time = begin_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public Date getBegin_time1() {
        return begin_time1;
    }

    public void setBegin_time1(Date begin_time1) {
        this.begin_time1 = begin_time1;
    }

    public Date getEnd_time1() {
        return end_time1;
    }

    public void setEnd_time1(Date end_time1) {
        this.end_time1 = end_time1;
    }

    public BaseQueryNative() {
    }

    ;

    public BaseQueryNative(int page, int pageSize) {
        this.page = page;
        this.pageSize = pageSize;
        this.startRow = (page - 1) * this.pageSize;
        this.endRow = this.startRow + this.pageSize - 1;
    }

    public int getStartRow() {
        return startRow;
    }

    public BaseQueryNative setStartRow(int startRow) {
        this.startRow = startRow;
        return this;
    }

    public int getEndRow() {
        return endRow;
    }

    public BaseQueryNative setEndRow(int endRow) {
        this.endRow = endRow;
        return this;
    }

    public int getPage() {
        return page;
    }

    public BaseQueryNative setPage(int page) {
        this.page = page;
        this.startRow = (page - 1) * this.pageSize;
        this.endRow = this.startRow + this.pageSize - 1;
        return this;
    }

    public int getPageSize() {
        return pageSize;
    }

    public BaseQueryNative setPageSize(int pageSize) {
        this.pageSize = pageSize;
        if (pageSize != DEFAULT_SIZE && page > 0) {
            this.startRow = (page - 1) * this.pageSize;
            this.endRow = this.startRow + this.pageSize - 1;
        }
        return this;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

}

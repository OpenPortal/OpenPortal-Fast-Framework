package cn.com.openportal.ffw.common.utils;

import java.util.List;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.common.utils
 * @create 2020-01-29 1:16
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
public interface BaseServiceNative<T, Q> {
    /**
     * 基本插入
     */
    Long addNative(T t);

    /**
     * 根据主键查询
     */
    T getByKeyNative(Long id);

    /**
     * 根据主键批量查询
     */
    List<T> getByKeysNative(List<Long> idList);

    /**
     * 根据主键删除
     */
    Integer deleteByKeyNative(Long id);

    /**
     * 根据条件删除
     */
    Integer deleteByQueryNative(Q q);

    /**
     * 根据主键批量删除
     */
    Integer deleteByKeysNative(List<Long> idList);

    /**
     * 删除所有
     */
    Integer deleteAllNative();

    /**
     * 根据主键更新非空字段
     */
    Integer updateByKeyNative(T t);

    /**
     * 根据主键更新所有字段
     */
    Integer updateByKeyAllNative(T t);

    /**
     * 根据条件分页查询
     */
    PageUtils getPageNative(Q q);

    /**
     * 根据条件查询
     */
    List<T> getListNative(Q q);

    /**
     * 根据条件查询总条数
     */
    Integer getCountNative(Q q);
}

package cn.com.openportal.ffw.common.annotation;

import java.lang.annotation.*;

/**
 * @author LeeSon QQ & WX:25901875
 * @version V1.0
 * @Package cn.com.openportal.ffw.common.annotation
 * @create 2020-01-09 11:34
 * @Copyright © 2019 LeeSon QQ & WX:25901875
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface CacheAspectAnnotation {
}

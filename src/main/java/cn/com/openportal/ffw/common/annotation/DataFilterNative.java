package cn.com.openportal.ffw.common.annotation;

import java.lang.annotation.*;

/**
 * 数据过滤
 *
 * @author LeeSon
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataFilterNative {
    /**  表的别名 */
    String tableAlias() default "";

    /**  true：必须自己创建 */
    boolean user() default false;

    /**  true：拥有子部门数据权限 */
    boolean subDept() default true;

    /**  部门ID */
    String deptId() default "dept_id";

    /**  用户ID */
    String userId() default "create_user_id";
}


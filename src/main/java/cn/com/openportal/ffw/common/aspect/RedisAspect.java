package cn.com.openportal.ffw.common.aspect;

import cn.com.openportal.ffw.common.exception.RRException;
import cn.com.openportal.ffw.cache.config.CacheType;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Redis切面处理类
 *
 * @author LeeSon
 */
@Aspect
@Component
public class RedisAspect {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    CacheType springCacheType;

    @Around("execution(* cn.com.openportal.ffw.common.utils.RedisUtils.*(..))")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Object result = null;
        if (springCacheType.getType() == CacheType.CACHE_IS_REDIS) {
            try {
                result = point.proceed();
            } catch (Exception e) {
                logger.error("redis error", e);
                throw new RRException("Redis服务异常");
            }
        }
        return result;
    }
}

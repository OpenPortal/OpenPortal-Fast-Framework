**项目说明** 
- OpenPortal-Fast-Framework是一个轻量级的，前后端分离的Java快速开发平台，能快速开发项目
- 支持MySQL、Oracle、SQL Server、PostgreSQL等主流数据库
- 官方网站：https://www.openportal.com.cn

<br> 

**具有如下特点** 
- 友好的代码结构及注释，便于阅读及二次开发
- 实现前后端分离，通过token进行数据交互，前端再也不用关注后端技术
- 灵活的权限控制，可控制到页面或按钮，满足绝大部分的权限需求
- 页面交互使用Vue2.x，极大的提高了开发效率
- 完善的代码生成机制，可在线生成entity、query、xml、dao、service、vue、sql代码，减少70%以上的开发任务
- 引入quartz定时任务，可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能
- 引入API模板，根据token作为登录令牌，极大的方便了APP接口开发
- 引入Hibernate Validator校验框架，轻松实现后端校验
- 引入云存储服务，已支持：七牛云、阿里云、腾讯云等
- 引入swagger文档支持，方便编写API接口文档
<br> 

**项目结构** 
```
OpenPortal-Fast-Framework
├─db  项目SQL语句
│
├─common 公共模块
│  ├─aspect 系统日志
│  ├─exception 异常处理
│  ├─validator 后台校验
│  └─xss XSS过滤
│ 
├─config 配置信息
│ 
├─modules 功能模块
│  ├─app API接口模块(APP调用)
│  ├─job 定时任务模块
│  ├─oss 文件服务模块
│  └─sys 权限模块
│ 
├─App 项目启动类
│  
├──resources 
│  ├─mapper SQL对应的XML文件
│  └─static 静态资源

```
<br> 

**如何交流、反馈、参与贡献？** 
- [官方网站](http://www.openportal.com.cn)：http://www.openportal.com.cn
- 官方QQ群：579395648
<br>

**技术选型：** 
- 核心框架：Spring Boot 2.1
- 安全框架：Apache Shiro 1.4
- 视图框架：Spring MVC 5.0
- 持久层框架：MyBatis 3.3
- 定时器：Quartz 2.3
- 数据库连接池：Druid 1.0
- 日志管理：SLF4J 1.7、Log4j
- 页面交互：Vue2.x 
<br>

 **后端部署**
- 通过git下载源码 https://gitee.com/OpenPortal
- idea、eclipse需安装lombok插件，不然会提示找不到entity的get set方法
- 创建数据库ffw,ffw-test，数据库编码为UTF-8
- 执行db/ffw.sql ffw-test.sql 文件，初始化数据
- 修改application-dev.yml，更新MySQL账号和密码
- Eclipse、IDEA运行App.java，则可启动项目

<br> 

 **前端部署**
 - 本项目是前后端分离的，还需要部署前端，才能运行起来
 - 前端下载地址：https://gitee.com/OpenPortal/OpenPortal-Fast-Framework-vue
 - 前端部署完毕，就可以访问项目了，账号：admin，密码：admin
 - 安装依赖
    * npm install -g cnpm --registry=https://registry.npm.taobao.org
    * cnpm install
 - 启动服务
    * npm run dev
 - 打包 & 发布
    * 构建生成的资源文件保存在/dist目录下，可通过config/index.js目录文件修改相关配置信息
    * 构建生产环境(默认)
        * npm run build
    * 构建测试环境
        * npm run build --qa
    * 构建验收环境
        * npm run build --uat
    * 构建生产环境
        * npm run build --prod
 - 常见问题
     * 开发时，如何连接后台项目api接口？
         * 修改/static/config/index.js目录文件中 window.SITE_CONFIG['baseUrl'] = '本地api接口请求地址';
     * 开发时，如何解决跨域？
         * 修改/config/dev.env.js目录文件中OPEN_PROXY: true开启代理
         * 修改/config/index.js目录文件中proxyTable对象target: '代理api接口请求地址'
         * 重启本地服务
    * 开发时，如何提前配置CDN静态资源？
         * 修改/static/config/index-[qa/uat/prod].js目录文件中window.SITE_CONFIG['domain'] = '静态资源cdn地址';
    * 构建生成后，发布需要上传哪些文件？
         * /dist目录下：1805021549（静态资源，18年05月03日15时49分）、config（配置文件）、index.html
    * 构建生成后，如何动态配置CDN静态资源？
         * 修改/dist/config/index.js目录文件中window.SITE_CONFIG['domain'] = '静态资源cdn地址';
    * 构建生成后，如何动态切换新旧版本？
         * 修改/dist/config/index.js目录文件中 window.SITE_CONFIG['version'] = '旧版本号';
 
<br>

 **项目演示**
- 演示地址：http://ffw.openportal.com.cn
- 账号密码：admin/admin

<br>
 
**Swagger接口**
- [系统原生](http://localhost:8080/openportal-ffw/swagger/index.html)：http://localhost:8080/openportal-ffw/swagger/index.html
- [业务模块](http://localhost:8080/openportal-ffw/swagger-ui.html)：http://localhost:8080/openportal-ffw/swagger-ui.html
